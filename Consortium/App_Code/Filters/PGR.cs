﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.App_Code.Filters
{
    public class PGR
    {
        public class SetTempDataModelStateAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(ActionExecutedContext filterContext)
            {
                base.OnActionExecuted(filterContext);
                filterContext.Controller.TempData["ModelState"] = filterContext.Controller.ViewData.ModelState;
            }
        }

        public class GetTempDataModelStateAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(ActionExecutedContext filterContext)
            {
                base.OnActionExecuted(filterContext);
                if(filterContext.Controller.TempData.ContainsKey("ModelState"))
                {
                    filterContext.Controller.ViewData.ModelState
                        .Merge((ModelStateDictionary)filterContext.Controller.TempData["ModelState"]);
                }
            }
        }
    }
}