﻿using Consortium.App_Code.Session;
using Consortium.App_Code.Session.Contract;
using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.App_Code.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AdminLayoutContentAttribute : ActionFilterAttribute
    {
        //protected ISession SessionHandler { get { return BaseSession.Instance; } }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //HttpSessionStateBase session = filterContext.HttpContext.Session;

            //if (filterContext.HttpContext.Request.IsAjaxRequest()
            //    || filterContext.Result is RedirectResult
            //    || null == SessionHandler.User) return;

            //if (null == filterContext.Controller.ViewData.Model)
            //{
            //    filterContext.Controller.ViewData.Model = new BaseViewModel();
            //}

            //BaseViewModel model = (BaseViewModel)filterContext.Controller.ViewData.Model;

            //model.User.Firstname = SessionHandler.User.Person.Firstname;
            //model.User.Lastname = SessionHandler.User.Person.Lastname;
            //model.User.Username = SessionHandler.User.Username;

            base.OnActionExecuted(filterContext);
        }
    }
}