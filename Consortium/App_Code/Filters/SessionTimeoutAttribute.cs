﻿using Consortium.App_Code.Base;
using Consortium.App_Code.Session;
using Consortium.App_Code.Session.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static Consortium.App_Code.Base._AbstractController;

namespace Consortium.App_Code.Filters
{
    [AttributeUsage(AttributeTargets.Class | 
        AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        protected ISession SessionHandler { get { return BaseSession.Instance; } }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;

            _AbstractController controller = (_AbstractController)filterContext.Controller;

            if (session.IsNewSession || SessionHandler.User == null)
            {
                if(filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    ResponseMessageBuilder response = new ResponseMessageBuilder();

                    controller.ResponseStatus(_AbstractController.RESPONSE_SESSION_TIMEOUT_CODE, "Session Timeout");
                    filterContext.Result = new JsonResult() {
                        Data = response.NoOk()
                        .Message("Session Timeout")
                        .AddProperty("RedirectTo", controller.Url.Action("Account", "Home", new { Area = String.Empty, returnUrl = filterContext.HttpContext.Request.RawUrl }))
                        .Build()
                    };
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                            { "Controller", "Home" },
                            { "Action", "Account" },
                            { "Area", String.Empty },
                            { "returnUrl", filterContext.HttpContext.Request.RawUrl}
                    });
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}