﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Consortium.App_Code.Session.Contract
{
    public interface ISession
    {
        User User { get; set; }

        void Logout();
    }
}
