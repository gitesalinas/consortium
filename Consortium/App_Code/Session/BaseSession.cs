﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Consortium.App_Code.Session.Contract;
using Tier.Entity;

namespace Consortium.App_Code.Session
{
    public class BaseSession : ISession
    {
        #region Singleton
        private static BaseSession _instance;
        public static BaseSession Instance { get { return _instance ?? (_instance = new BaseSession()); } }

        private BaseSession()
        {

        }
        #endregion
        
        public User User
        {
            get { return (User)HttpContext.Current.Session["User"]; }
            set { HttpContext.Current.Session["User"] = value; }
        }

        public void Logout()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.Clear();
        }
    }
}