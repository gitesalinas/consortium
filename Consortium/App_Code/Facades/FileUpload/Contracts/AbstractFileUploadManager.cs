﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Helper.File;
using Tier.Service;

namespace Consortium.App_Code.Facades.FileUpload.Contracts
{
    public abstract class AbstractFileUploadManager
    {
        public int Id { get; set; }
        public Resource Resource { get; set; }
        protected FileHelper _fileHelper { get; set; }
        protected ResourceService ResourceService { get { return ResourceService.Instance; } }

        public AbstractFileUploadManager(FileHelper fileHelper)
        {
            this._fileHelper = fileHelper;
        }

        public abstract void Prepare();

        public virtual void Save()
        {
            _fileHelper.Save();
        }
    }
}