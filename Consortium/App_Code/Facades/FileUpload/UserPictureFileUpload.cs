﻿using Consortium.App_Code.Facades.FileUpload.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Helper.File;
using Tier.Service;
using static Tier.Library.Helper.File.ImageFileHelper;

namespace Consortium.App_Code.Facades.FileUpload
{
    public class UserPictureFileUpload : AbstractFileUploadManager
    {
        private const string SUFFIX_PATH = "User";

        public ImageFileHelper FileHelper { get { return (ImageFileHelper)_fileHelper; } }

        //private User UserLogged { get; set; }
        private User User { get; set; }
        private UserService UserService { get { return UserService.Instance; } }

        public UserPictureFileUpload(int userId, FileHelper fileHelper) 
            : base(fileHelper)
        {
            User = UserService.Get(userId);

            if (null == User || 0 >= User.Id) throw new Exception("User not found");

            //UserLogged = userLogged;
        }

        public override void Prepare()
        {
            Resource = (User.PictureId.HasValue ? ResourceService.Get(User.PictureId.Value) : new Resource());
            Resource = Resource ?? new Resource();

            if (!String.IsNullOrEmpty(Resource.PublicName)) _fileHelper.FilePublicName = Resource.PublicName;

            FileHelper.SetThumbnailSize(EnumThumbnailSize.Small);
            FileHelper.CreatePaths();
            FileHelper.AppendUriToPaths(SUFFIX_PATH);
        }

        public override void Save()
        {
            FileHelper.SaveWithThumbnail();

            Resource.Name = FileHelper.FileName;
            Resource.Extension = FileHelper.FileName;
            Resource.ContentLength = FileHelper.FileContentLength;
            Resource.ContentType = FileHelper.FileContentType;

            Resource.PublicName = FileHelper.FilePublicName;

            Resource.DirectoryPath = FileHelper.FileDirectoryPath;
            Resource.Path = FileHelper.FilePath;

            Resource.VirtualDirectoryPath = FileHelper.FileVirtualDirectoryPath;
            Resource.VirtualPath = FileHelper.FileVirtualPath;

            Resource.ThumbnailDirectoryPath = FileHelper.FileThumbnailDirectoryPath;
            Resource.ThumbnailPath = FileHelper.FileThumbnailPath;

            Resource.ThumbnailVirtualDirectoryPath = FileHelper.FileThumbnailVirtualDirectoryPath;
            Resource.ThumbnailVirtualPath = FileHelper.FileThumbnailVirtualPath;

            Resource.ResolveSecurityFields(User.Id);
            Resource.SetEnabled();

            UserService.SaveProfilePicture(User, Resource);

            //if(null != UserLogged && 0 < UserLogged.Id && UserLogged.Id == User.Id)
            //{
            //    UserLogged.Picture = Resource;
            //}
        }
    }
}