﻿using Consortium.App_Code.Facades.FileUpload.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Helper.File;
using Tier.Service;
using static Tier.Library.Helper.File.ImageFileHelper;

namespace Consortium.App_Code.Facades.FileUpload
{
    public class RoomMediaFileUpload : AbstractFileUploadManager
    {
        private const string SUFFIX_PATH = "Room";

        public ImageFileHelper FileHelper { get { return (ImageFileHelper)_fileHelper; } }

        public int UserId { get; set; }
        public Room Room { get; set; }
        public RoomMedia RoomMedia { get; set; }
        public RoomService RoomService { get { return RoomService.Instance; } }
        public RoomMediaService RoomMediaService { get { return RoomMediaService.Instance; } }

        public RoomMediaFileUpload(int userId, int roomId, FileHelper fileHelper) 
            : base(fileHelper)
        {
            Room = RoomService.Get(roomId);
            if(null == Room || 0 >= Room.Id) throw new Exception("Room not found");

            RoomMedia = new RoomMedia() { RoomId = roomId };
            RoomMedia.ResolveSecurityFields(UserId);
            RoomMedia.SetEnabled();
            UserId = userId;
        }

        public override void Save()
        {
            FileHelper.SaveWithThumbnail();

            Resource.Name = FileHelper.FileName;
            Resource.Extension = FileHelper.FileName;
            Resource.ContentLength = FileHelper.FileContentLength;
            Resource.ContentType = FileHelper.FileContentType;

            Resource.PublicName = FileHelper.FilePublicName;

            Resource.DirectoryPath = FileHelper.FileDirectoryPath;
            Resource.Path = FileHelper.FilePath;

            Resource.VirtualDirectoryPath = FileHelper.FileVirtualDirectoryPath;
            Resource.VirtualPath = FileHelper.FileVirtualPath;

            Resource.ThumbnailDirectoryPath = FileHelper.FileThumbnailDirectoryPath;
            Resource.ThumbnailPath = FileHelper.FileThumbnailPath;

            Resource.ThumbnailVirtualDirectoryPath = FileHelper.FileThumbnailVirtualDirectoryPath;
            Resource.ThumbnailVirtualPath = FileHelper.FileThumbnailVirtualPath;

            Resource.ResolveSecurityFields(UserId);
            Resource.SetEnabled();

            RoomMedia.Resource = Resource;

            //RoomService.AddRoomMedia(RoomMedia);
            RoomMediaService.AddRoomMedia(RoomMedia);

            Id = RoomMedia.Id;
        }

        public override void Prepare()
        {
            Resource = new Resource();

            FileHelper.SetThumbnailSize(EnumThumbnailSize.Large);
            FileHelper.CreatePaths();
            FileHelper.AppendUriToPaths(SUFFIX_PATH);
        }
    }
}