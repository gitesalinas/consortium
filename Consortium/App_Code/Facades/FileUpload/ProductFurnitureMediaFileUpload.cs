﻿using Consortium.App_Code.Facades.FileUpload.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Helper.File;
using Tier.Service;
using static Tier.Library.Helper.File.ImageFileHelper;

namespace Consortium.App_Code.Facades.FileUpload
{
    public class ProductFurnitureMediaFileUpload : AbstractFileUploadManager
    {
        private const string SUFFIX_PATH = "Product Furniture";

        public ImageFileHelper FileHelper { get { return (ImageFileHelper)_fileHelper; } }

        public int UserId { get; set; }
        public ProductFurniture Product { get; set; }
        public ProductFurnitureMedia ProductMedia { get; set; }
        public ProductFurnitureService ProductService { get { return ProductFurnitureService.Instance; } }
        public ProductFurnitureMediaService ProductMediaService { get { return ProductFurnitureMediaService.Instance; } }

        public ProductFurnitureMediaFileUpload(int userId, int productId, FileHelper fileHelper) 
            : base(fileHelper)
        {
            Product = ProductService.Get(productId);
            if (null == Product || 0 >= Product.Id) throw new Exception("Product not found");

            ProductMedia = new ProductFurnitureMedia() { ProductId = productId };
            ProductMedia.ResolveSecurityFields(UserId);
            ProductMedia.SetEnabled();
            UserId = userId;
        }

        public override void Save()
        {
            FileHelper.SaveWithThumbnail();

            Resource.Name = FileHelper.FileName;
            Resource.Extension = FileHelper.FileExtension;
            Resource.ContentLength = FileHelper.FileContentLength;
            Resource.ContentType = FileHelper.FileContentType;

            Resource.PublicName = FileHelper.FilePublicName;

            Resource.DirectoryPath = FileHelper.FileDirectoryPath;
            Resource.Path = FileHelper.FilePath;

            Resource.VirtualDirectoryPath = FileHelper.FileVirtualDirectoryPath;
            Resource.VirtualPath = FileHelper.FileVirtualPath;

            Resource.ThumbnailDirectoryPath = FileHelper.FileThumbnailDirectoryPath;
            Resource.ThumbnailPath = FileHelper.FileThumbnailPath;

            Resource.ThumbnailVirtualDirectoryPath = FileHelper.FileThumbnailVirtualDirectoryPath;
            Resource.ThumbnailVirtualPath = FileHelper.FileThumbnailVirtualPath;

            Resource.ResolveSecurityFields(UserId);
            Resource.SetEnabled();

            ProductMedia.Resource = Resource;

            //ProductService.AddProductMedia(ProductMedia);
            ProductMediaService.AddRoomMedia(ProductMedia);

            Id = ProductMedia.Id;
        }

        public override void Prepare()
        {
            Resource = new Resource();

            FileHelper.SetThumbnailSize(EnumThumbnailSize.Large);
            FileHelper.CreatePaths();
            FileHelper.AppendUriToPaths(SUFFIX_PATH);
        }
    }
}