﻿using Consortium.App_Code.Session;
using Consortium.App_Code.Session.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tier.Library.Common;

namespace Consortium.App_Code.Base
{
    public class _AbstractController : Controller
    {
        public const int RESPONSE_ERROR_CODE = 555;
        public const int RESPONSE_SESSION_TIMEOUT_CODE = 556;

        public const string RESPONSE_MESSAGE_RESULT_KEY = "Result";
        public const string RESPONSE_MESSAGE_TYPE_SUCCESS = "Success";
        public const string RESPONSE_MESSAGE_TYPE_ERROR = "Error";
        public const string RESPONSE_MESSAGE_TYPE_INFO = "Info";
        public const string RESPONSE_MESSAGE_TYPE_WARNING = "Warning";

        protected ISession SessionHandler { get { return BaseSession.Instance; } }

        public void ResponseError(string description)
        {
            ResponseStatus(RESPONSE_ERROR_CODE, description);
        }

        public void ResponseStatus(int code, string description)
        {
            Response.StatusCode = code;
            Response.StatusDescription = description;
        }

        protected void PGRMessage(object dataMessage)
        {
            TempData["DataMessage"] = dataMessage;
        }

        public class ResponseMessageBuilder
        {
            private string Result { get; set; }
            private List<object> Messages = new List<object>();
            private Dictionary<string, string> MessageValues = new Dictionary<string, string>();

            private Dictionary<string, object> Properties = new Dictionary<string, object>();

            //private List<object> ModelStateMessages = new List<object>();

            public ResponseMessageBuilder()
            {
                Messages.Add(MessageValues);
            }

            public ResponseMessageBuilder Ok() { Result = "Ok"; return this; }
            public ResponseMessageBuilder NoOk() { Result = "NoOk"; return this; }
            public ResponseMessageBuilder Success() { MessageValues["MessageType"] = RESPONSE_MESSAGE_TYPE_SUCCESS; return this; }
            public ResponseMessageBuilder Info() { MessageValues["MessageType"] = RESPONSE_MESSAGE_TYPE_INFO; return this; }
            public ResponseMessageBuilder Warning() { MessageValues["MessageType"] = RESPONSE_MESSAGE_TYPE_WARNING; return this; }
            public ResponseMessageBuilder Error() { MessageValues["MessageType"] = RESPONSE_MESSAGE_TYPE_ERROR; return this; }
            public ResponseMessageBuilder Message(string message) { MessageValues["Message"] = message; return this; }
            public ResponseMessageBuilder AddProperty(string key, object value) { Properties[key] = value; return this; }
            public ResponseMessageBuilder AddNewMessage()
            {
                MessageValues = new Dictionary<string, string>();
                Messages.Add(MessageValues);

                return this;
            }

            //public ResponseMessageBuilder ModelStateErrors(ModelStateDictionary modelState)
            //{
            //    foreach (var item in modelState)
            //    {
            //        if (item.Value.Errors.Count > 0)
            //        {
            //            List<object> errors = new List<object>();

            //            foreach (var error in item.Value.Errors)
            //            {
            //                //ModelStateMessages.Add(new { Field = item.Key, Message = error.ErrorMessage });
            //                errors.Add(new { Message = error.ErrorMessage });
            //                //sb.AppendLine($"<strong>{item.Key}</strong> <small>({error.ErrorMessage})</small><br />");
            //            }

            //            ModelStateMessages.Add(new { Field = item.Key, Errors = errors });
            //        }
            //    }

            //return this;//Warning();//Message(sb.ToString()).Warning();
            //}

            public ResponseMessageBuilder ModelStateErrorsString(ModelStateDictionary modelState)
            {
                StringBuilder sb = new StringBuilder("<br />");

                foreach (var item in modelState)
                {
                    if (item.Value.Errors.Count > 0)
                    {
                        foreach (var error in item.Value.Errors)
                        {
                            sb.AppendLine($"<strong>{item.Key}</strong> <small>({error.ErrorMessage})</small><br />");
                        }
                    }
                }

                Message(sb.ToString());

                return this;
            }

            public void Clear()
            {
                Result = null;
                MessageValues = new Dictionary<string, string>();
                Messages.Clear();
                //ModelStateMessages.Clear();
                Properties = new Dictionary<string, object>();
                Messages.Add(MessageValues);
            }

            public object Build()
            {
                Dictionary<string, object> responseValues = new Dictionary<string, object>();

                responseValues["Result"] = Result;

                if (0 < Messages.Count && ((Dictionary<string, string>)Messages[Messages.Count - 1]).Keys.Count > 0)
                    responseValues["Messages"] = new List<object>(Messages);

                //if (0 < ModelStateMessages.Count)
                //responseValues["ModelStateMessages"] = new List<object>(ModelStateMessages);

                if (Properties.Keys.Count > 0)
                    Properties.ToList().ForEach(x => responseValues.Add(x.Key, x.Value));

                Clear();

                return responseValues;
            }
        }
    }
}