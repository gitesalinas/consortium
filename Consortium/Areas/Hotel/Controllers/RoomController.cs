﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Library.Common;
using Tier.Service;

namespace Consortium.Areas.Hotel.Controllers
{
    [SessionTimeout]
    public partial class RoomController : _AbstractController
    {
        private RoomService _roomService { get { return RoomService.Instance; } }
        private StatusService _statusService { get { return StatusService.Instance; } }
        private FacilityService _facilityService { get { return FacilityService.Instance; } }
        private RoomFacilityService _roomFacilityService { get { return RoomFacilityService.Instance; } }
        private RoomMediaService _roomMediaService { get { return RoomMediaService.Instance; } }

        // GET: Hotel/Room
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int? id)
        {
            Room room = id.HasValue && 0 < id.Value ? _roomService.Get(id.Value) : new Room();

            RoomViewModel model = Mapper.Map<Room, RoomViewModel>(room);
            model = model ?? new RoomViewModel();

            if (id.HasValue)
            {
                RoomMedia roomMedia = _roomMediaService.GetMainRoomMedia(room.Id);
                if (null != roomMedia && 0 < roomMedia.Id)
                {
                    model.ImageUrl = roomMedia.Resource.ThumbnailVirtualPath;
                }
            }

            return View(model);
        }
    }

    [SessionTimeout]
    public partial class RoomController : _AbstractController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Detail(int? id, RoomViewModel model)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            Room room = id.HasValue && 0 < id.Value ? _roomService.Get(id.Value) : new Room();

            room.Name = model.Name;
            room.Description = model.Description;
            room.FullDescription = model.FullDescription;
            room.Price = Convert.ToDecimal(model.Price);
            room.Status = Convert.ToInt16(model.StatusId);
            room.ResolveSecurityFields(SessionHandler.User.Id);

            _roomService.Save(room);

            if (!id.HasValue || id.Value <= 0)
            {
                PGRMessage(responseMessageBuilder.Ok().Success().Message("Room info save successfully").Build());

                responseMessageBuilder.Clear();
                responseMessageBuilder.AddProperty("RedirectTo", Url.Action("Detail", "Room", new { id = room.Id }));
            }
            else
            {
                responseMessageBuilder.Ok().Success().Message("Room info save successfully");
            }

            return Json(responseMessageBuilder.Build(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult HandleFacility(int id, int facilityId)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning().Message("Invalid room id").Build(),
                    JsonRequestBehavior.AllowGet);
            }

            if (0 >= facilityId)
            {
                return Json(responseMessageBuilder.NoOk().Warning().Message("Invalid facility id").Build(),
                    JsonRequestBehavior.AllowGet);
            }

            RoomFacility roomFacility = _roomFacilityService.GetAnyByRoomIdAndFacilityId(id, facilityId);

            string action = String.Empty;
            if (null == roomFacility)
            {
                roomFacility = new RoomFacility() { RoomId = id, FacilityId = facilityId };
                roomFacility.SetEnabled();
                action = "saved";
            }
            else if (roomFacility.Status.Equals((short)EnumStatus.Deleted)) { roomFacility.SetEnabled(); action = "saved"; }
            else { roomFacility.SetDeleted(); action = "deleted"; }

            roomFacility.ResolveSecurityFields(SessionHandler.User.Id);

            _roomFacilityService.Save(roomFacility);

            return Json(responseMessageBuilder.Ok().Success().Message($"Facility has been {action} successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid room id").Build(), JsonRequestBehavior.AllowGet);
            }

            Room room = _roomService.Get(id);

            if (null == room || 0 >= room.Id)
            {
                return Json(responseMessageBuilder.NoOk().Info()
                  .Message("Room not found").Build(), JsonRequestBehavior.AllowGet);
            }

            room.ResolveSecurityFields(SessionHandler.User.Id);
            room.SetDeleted();

            _roomService.Delete(room);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("User deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteRoomMedia(int id)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid room media id").Build(), JsonRequestBehavior.AllowGet);
            }

            RoomMedia roomMedia = _roomMediaService.Get(id);

            if (null == roomMedia || 0 >= roomMedia.Id)
            {
                return Json(responseMessageBuilder.NoOk().Info()
                  .Message("Room media not found").Build(), JsonRequestBehavior.AllowGet);
            }

            roomMedia.ResolveSecurityFields(SessionHandler.User.Id);
            roomMedia.SetDeleted();

            _roomMediaService.Delete(roomMedia);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Room media deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        ///Admin/Hotel/Room/SetMainRoomMedia/3
        [HttpPost]
        public JsonResult HandleMainRoomMedia(int id, int roomMediaId)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid room id").Build(), JsonRequestBehavior.AllowGet);
            }

            if (0 >= roomMediaId)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid room media id").Build(), JsonRequestBehavior.AllowGet);
            }

            RoomMedia roomMedia = _roomMediaService.Get(roomMediaId);

            if (null == roomMedia || 0 >= roomMedia.Id)
            {
                return Json(responseMessageBuilder.NoOk().Info()
                  .Message("Room media not found").Build(), JsonRequestBehavior.AllowGet);
            }

            roomMedia.RoomId = id;
            roomMedia.ResolveSecurityFields(SessionHandler.User.Id);

            _roomMediaService.HandleMain(roomMedia);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Room media main setted successfully").Build(), JsonRequestBehavior.AllowGet);
        }


    }
}