﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using Consortium.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Hotel.Controllers
{
    [SessionTimeout]
    public partial class _RenderActionController : _AbstractController
    {
        private RoomService _roomService { get { return RoomService.Instance; } }
        private FacilityService _facilityService { get { return FacilityService.Instance; } }
        private RoomFacilityService _roomFacilityService { get { return RoomFacilityService.Instance; } }
        private RoomMediaService _roomMediaService { get { return RoomMediaService.Instance; } }

        //[ChildActionOnly]
        public ActionResult RoomsList(string option = null)
        {
            IEnumerable<Room> rooms = _roomService.GetAll();

            IEnumerable<RoomViewModel> model = Mapper.Map<IEnumerable<Room>, IEnumerable<RoomViewModel>>(rooms);

            ViewBag.Option = option;

            return PartialView("~/Areas/Hotel/Views/Room/_PartialRoomsList.cshtml", model);
        }

        public ActionResult RoomFacilities(int id, string option)
        {
            RoomFacilitiesViewModel model = new RoomFacilitiesViewModel() { Option = option };

            IEnumerable<Facility> facilities = _facilityService.GetAll();
            facilities = facilities ?? new List<Facility>();

            model.Facilities = Mapper.Map<IEnumerable<Facility>, IEnumerable<SelectListItem>>(facilities);

            //IEnumerable<FacilityViewModel> facilityViewModel = Mapper.Map<IEnumerable<Facility>, IEnumerable<FacilityViewModel>>(facilities);

            IEnumerable<RoomFacility> roomFacilities = _roomFacilityService.GetAllByRoomId(id);
            roomFacilities = roomFacilities ?? new List<RoomFacility>();

            model.RoomFacilities = (from rf in roomFacilities select rf.FacilityId).ToList();

            //IEnumerable<RoomFacilityViewModel> roomFacilitiesViewModel = Mapper.Map<IEnumerable<RoomFacility>, IEnumerable<RoomFacilityViewModel>>(roomFacilities);

                                       /*var facilities2 = (from f in facilityViewModel
                                                          join rf in facilityViewModel on f.Id equals rf.Id into xx
                                                          from frf in xx.DefaultIfEmpty()
                                                          select new SelectListItem
                                                          {
                                                              Selected = f.Id.Equals(frf.Id)
                                                          }).ToList();*/

            return PartialView("~/Areas/Hotel/Views/Room/_PartialRoomFacilities.cshtml", model);
        }


        public ActionResult RoomMedias(int id, string option)
        {
            RoomMediasViewModel model = new RoomMediasViewModel() { Option = option };

            IEnumerable<RoomMedia> roomMedias = _roomMediaService.GetAllByRoomId(id);
            roomMedias = roomMedias ?? new List<RoomMedia>();

            model.RoomMedias = Mapper.Map<IEnumerable<RoomMedia>, IEnumerable<RoomMediaViewModel>>(roomMedias);

            return PartialView("~/Areas/Hotel/Views/Room/_PartialRoomMedias.cshtml", model);
        }

    }
}