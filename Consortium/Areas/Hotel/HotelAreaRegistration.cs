﻿using System.Web.Mvc;

namespace Consortium.Areas.Hotel
{
    public class HotelAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Hotel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_Hotel_default",
                "Admin/Hotel/{controller}/{action}/{id}",
                new { action = "Room", id = UrlParameter.Optional }
            );
        }
    }
}