﻿; (function ($, window, document, undefined, util, ajaxAware) {
    var params = {};

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            if (params.roomInfoForm) {
                params.roomInfoForm.$.validate(params.roomInfoFormValidations);

                if (params.roomFacilities) params.roomFacilities.init();
                if (params.roomMediaColorbox) params.roomMediaColorbox.init();
                if (params.roomMediaDropzone) params.roomMediaDropzone.init();
            }

            if (params.roomsTable)
                params.roomsTable.init();
        },
        _saveRoomInfo: function ($ele) {

            ajaxAware.subscribe($ele[0], params.roomInfoForm.ajaxLoader);

            var ajax = util.ajaxCall(params.roomInfoForm.action, params.roomInfoForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                if (typeof data.RedirectTo !== "undefined") {
                    window.location.href = data.RedirectTo;
                    return;
                }

                params.roomInfoForm.messenger.showMessage(data.Messages[0]);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.roomInfoForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });

            return false;
        },
        _handleRoomFacility: function ($ele, id) {

            ajaxAware.subscribe($ele[0], params.roomFacilityForm.ajaxLoader);

            var ajax = util.ajaxCallContent(params.roomFacilityForm.action.replace('_facilityId_', id));

            ajax.done(function (data, textStatus, jqXHR) {

                params.roomFacilityForm.messenger.showMessage(data.Messages[0]);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.roomFacilityForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _delete: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteRoomForm.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteRoomForm.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteRoomForm.messenger.showMessage(data.Messages[0]);
                params.deleteModal.hide();

                that._load(params.loadRoomsForm.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteRoomForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _load: function ($ele) {

            params.roomsTable.destroy();

            params.roomsTable.clearBody();

            ajaxAware.subscribe($ele[0], params.loadRoomsTable.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadRoomsTable.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadRoomsTable.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserForm.messenger);
            }).always(function (data, textStatus, jqXHR) { params.roomsTable.init(); ajaxAware.unsubscribe($ele[0]); });
        },

        _deleteRoomMedia: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteRoomMediaGrid.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteRoomMediaGrid.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteRoomMediaGrid.messenger.showMessage(data.Messages[0]);
                params.deleteRoomMediaModal.hide();

                that._loadRoomMedias(params.loadRoomMedia.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteRoomMediaGrid.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },

        _loadRoomMedias: function ($ele) {

            ajaxAware.subscribe($ele[0], params.loadRoomMedia.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadRoomMedia.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadRoomMedia.$.html(data);

                if (params.loadRoomMedia.callback) params.loadRoomMedia.callback();

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadRoomMedia.messenger);
            }).always(function (data, textStatus, jqXHR) { params.roomMediaColorbox.init(); ajaxAware.unsubscribe($ele[0]); });
        },

        _handleMainRoomMedia: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.handleMainRoomMedia.ajaxLoader);

            var ajax = util.ajaxCall(params.handleMainRoomMedia.action, { roomMediaId: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.handleMainRoomMedia.messenger.showMessage(data.Messages[0]);

                that._loadRoomMedias(params.loadRoomMedia.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadRoomMedia.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        }
    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        saveRoomInfo: function (ele) {

            if (!params.roomInfoForm.$.valid()) {
                return;
            }

            return functions._saveRoomInfo($(ele));
        },
        confirmDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteModal.setHandlers("$.fn.roomJS('delete', this, " + id + ")", "");
            params.deleteModal.show();
        },
        delete: function (ele, id) {

            if (!isId(id)) return;

            functions._delete($(ele), id);
        },

        addRoomFacility: function (ele, value) {

            var id = value;

            if (!isId(id)) return;

            functions._handleRoomFacility($(ele), id);
        },
        dropRoomFacility: function (ele, value) {
            var id = value;

            if (!isId(id)) return;

            functions._handleRoomFacility($(ele), id);
        },

        uploadAllMediaFiles: function (ele) {
            params.roomMediaDropzone.uploadAllFiles();
        },
        confirmRoomMediaDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteRoomMediaModal.setHandlers("$.fn.roomJS('deleteRoomMedia', this, " + id + ")", "");
            params.deleteRoomMediaModal.show();
        },
        deleteRoomMedia: function (ele, id) {
            if (!isId(id)) return;

            functions._deleteRoomMedia($(ele), id);
        },

        loadRoomMedias: function (ele) {
            functions._loadRoomMedias($(ele));
        },

        handleMainRoomMedia: function (ele, id) {
            if (!isId(id)) return;

            functions._handleMainRoomMedia($(ele), id);
        }
    };

    $.fn.roomJS = function (parameter) {

        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.roomJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWAjaxAware);