﻿using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Hotel.ViewModels
{
    public class RoomViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Name is a required field")]
        public string Name { get; set; }
        public string Description { get; set; }
        [AllowHtml]
        public string FullDescription { get; set; }

        [Required(ErrorMessage = "Price is a required field")]
        public string Price { get; set; }

        public string StatusId { get; set; }
        public string Status { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_imageUrl)) return DefaultImageUrl;
                return $"{_imageUrl}?t={(long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds}";
            }
            set
            {
                _imageUrl = value;
            }
        }

        public string DefaultImageUrl { get { return VirtualPathUtility.ToAbsolute("~/Content/assets/images/default-image.png"); } }

        public List<FacilityViewModel> RoomFacilities = new List<FacilityViewModel>();
        public List<RoomMediaViewModel> RoomMedias = new List<RoomMediaViewModel>();
    }
}