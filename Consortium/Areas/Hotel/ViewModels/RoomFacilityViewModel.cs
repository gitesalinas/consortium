﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Hotel.ViewModels
{
    public class RoomFacilityViewModel
    {
        public int RoomId { get; set; }
        public RoomViewModel Room { get; set; }

        public int FacilityId { get; set; }
        public FacilityViewModel Facility { get; set; }
    }

    public class RoomFacilitiesViewModel
    {
        public string Option { get; set; }
        public List<int> RoomFacilities { get; set; }
        public IEnumerable<SelectListItem> Facilities { get; set; }
    }
}