﻿using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Hotel.ViewModels
{
    public class RoomMediaViewModel : BaseViewModel
    {
        //public string Text { get; set; }
        public bool IsMain { get; set; }

        public int RoomId { get; set; }
        public RoomViewModel Room { get; set; }

        public int ResourceId { get; set; }
        public ResourceViewModel Resource { get; set; }

        public short StatusId { get; set; }
        public string Status { get; set; }
    }

    public class RoomMediasViewModel
    {
        public string Option { get; set; }
        public IEnumerable<RoomMediaViewModel> RoomMedias { get; set; }
    }
}