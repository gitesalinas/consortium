﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Tier.Entity;
using Tier.Library.Common;
using Tier.Service;

namespace Consortium.Areas.UserAPI.Controllers
{
    public class APIController : ApiController
    {
        private UserService UserService { get { return UserService.Instance; } }

        [HttpGet]
        public object Authenticate()
        {
            try
            {
                //string param = Request.Headers.GetValues("eder").FirstOrDefault();
                if (null == Request.Headers.Authorization) throw new Exception("Authorization Header Not Found");

                string authorization = Request.Headers.Authorization.Parameter;

                try
                {
                    string[] credentials = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(authorization)).Split(':');

                    return new User() { Username = credentials[0], Password = credentials[1], Status = (short)EnumStatus.Enabled };
                }
                catch (Exception)
                {
                    throw new Exception("Malformed Authorization Header");
                }
                //return UserService.Login(credentials[0], credentials[1]);
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest).ReasonPhrase = ex.Message;
            }
        }
    }
}
