﻿using System.Web.Http;
using System.Web.Mvc;

namespace Consortium.Areas.UserAPI
{
    public class UserAPIAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UserAPI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //****************=======Default Api Route=========*******************
            context.Routes.MapHttpRoute(
                name: "UserAPI_default",
                routeTemplate: "UserAPI/{controller}/{action}/{id}",
                defaults: new { controller = "API", action = "Index", id = UrlParameter.Optional }
            );

            /*context.Routes.MapHttpRoute(
                name: "AdminPanelApi",
                routeTemplate: "AdminPanel/api/{controller}"
            );*/

            //****************=======Default Route=========*******************

            /*context.MapRoute(
                "UserAPI_default",
                "UserAPI/{controller}/{action}/{id}",
                new { controller = "API", action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}