﻿; (function ($, window, document, undefined, util, plugins, ajaxAware) {
    var params = {};

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            if (params.accountInfoForm) {
                params.accountInfoForm.$.validate(params.accountInfoFormValidations);
                params.userBirthDatePicker.init();
                params.userPictureFileUpload.init();
                //plugins.fileUpload("#fPicture", params.accountInfoForm.messenger);
            }

            if (params.usersTable)
                params.usersTable.init();
        },
        _saveAccountInfo: function ($ele) {

            ajaxAware.subscribe($ele[0], params.accountInfoForm.ajaxLoader);

            var ajax = util.ajaxCall(params.accountInfoForm.action, params.accountInfoForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                if (typeof data.RedirectTo !== "undefined") {
                    window.location.href = data.RedirectTo;
                    return;
                }

                params.accountInfoForm.messenger.showMessage(data.Messages[0]);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.accountInfoForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });

            return false;
        },
        _delete: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteUserForm.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteUserForm.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteUserForm.messenger.showMessage(data.Messages[0]);
                params.deleteModal.hide();

                that._load(params.loadUsersForm.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _load: function ($ele) {

            params.userTable.destroy();

            params.userTable.clearBody();

            ajaxAware.subscribe($ele[0], params.loadUsersForm.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadUsersForm.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadUsersForm.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserForm.messenger);
            }).always(function (data, textStatus, jqXHR) { params.userTable.init(); ajaxAware.unsubscribe($ele[0]); });
        }
    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        saveAccountInfo: function (ele) {

            if (!params.accountInfoForm.$.valid()) {
                return;
            }

            return functions._saveAccountInfo($(ele));
        },
        confirmDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteModal.setHandlers("$.fn.userJS('delete', this, " + id + ")", "");
            params.deleteModal.show();
        },
        delete: function (ele, id) {

            if (!isId(id)) return;

            functions._delete($(ele), id);
        }
    };

    $.fn.userJS = function (parameter) {

        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.userJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWPlugins, ESAWAjaxAware);