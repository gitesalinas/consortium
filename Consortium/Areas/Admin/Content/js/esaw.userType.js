﻿; (function ($, window, document, undefined, util, plugins, ajaxAware) {
    var params = {};

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            if (params.userTypeForm) {
                params.userTypeForm.$.validate(params.userTypeFormValidations);
            }

            if (params.userTypesTable)
                params.userTypesTable.init();
        },
        _saveUserType: function ($ele) {
            var that = this;
            ajaxAware.subscribe($ele[0], params.userTypeForm.ajaxLoader);

            var ajax = util.ajaxCall(params.userTypeForm.action, params.userTypeForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                params.userTypeForm.messenger.showMessage(data.Messages[0]);
                params.saveModal.hide();
                that._loadList(params.loadUserTypesTable.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.userTypeForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });

            return false;
        },
        _loadBy: function ($ele, id) {

            ajaxAware.subscribe($ele[0], params.loadUserTypeForm.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadUserTypeForm.action.replace('_id_', id));

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadUserTypeForm.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadUserTypeForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _delete: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteUserTypeForm.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteUserTypeForm.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteUserTypeForm.messenger.showMessage(data.Messages[0]);
                params.deleteModal.hide();

                that._loadList(params.loadUserTypesTable.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserTypeForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _loadList: function ($ele) {

            params.userTypesTable.destroy();

            params.userTypesTable.clearBody();

            ajaxAware.subscribe($ele[0], params.loadUserTypesTable.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadUserTypesTable.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadUserTypesTable.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserTypeForm.messenger);
            }).always(function (data, textStatus, jqXHR) { params.userTypesTable.init(); ajaxAware.unsubscribe($ele[0]); });
        }
    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        saveUserType: function (ele) {

            if (!params.userTypeForm.$.valid()) {
                return;
            }

            return functions._saveUserType($(ele));
        },
        loadBy: function (ele, id) {

            params.saveModal.show();

            //if (!isId(id)) return;

            functions._loadBy($(ele), id);
        },
        loadList: function (ele) {
            functions._loadList((!ele ? params.loadUserTypesTable.$ : $(ele)));
        },
        confirmDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteModal.setHandlers("$.fn.userTypeJS('delete', this, " + id + ")", "");
            params.deleteModal.show();
        },
        delete: function (ele, id) {

            if (!isId(id)) return;

            functions._delete($(ele), id);
        }
    };

    $.fn.userTypeJS = function (parameter) {

        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.userTypeJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWPlugins, ESAWAjaxAware);