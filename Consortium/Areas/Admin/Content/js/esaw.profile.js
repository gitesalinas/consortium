﻿; (function ($, window, document, undefined, util, plugins, ajaxAware) {
    var params = {};

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            params.accountInfoForm.$.validate(params.accountInfoFormValidations);
            params.userBirthDatePicker.init();
            params.userPictureFileUpload.init();
            //plugins.fileUpload("#fPicture", params.accountInfoForm.messenger);
        },
        _saveAccountInfo: function ($ele) {

            ajaxAware.subscribe($ele, params.accountInfoForm.ajaxLoader);

            var ajax = util.ajaxCall(params.accountInfoForm.action, params.accountInfoForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                params.accountInfoForm.messenger.showMessage(data.Messages[0]);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.accountInfoForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele); });

            return false;
        }
    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        saveAccountInfo: function (ele) {

            if (!params.accountInfoForm.$.valid()) {
                return;
            }

            return functions._saveAccountInfo($(ele));
        }
    };

    $.fn.profileJS = function (parameter) {

        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.profileJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWPlugins, ESAWAjaxAware);