﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Admin.Controllers
{
    [SessionTimeout]
    public partial class _RenderActionController : _AbstractController
    {
        private UserService _userService;
        private UserTypeService _userTypeService;
        private StatusService _statusService;

        public _RenderActionController()
        {
            _userService = UserService.Instance;
            _userTypeService = UserTypeService.Instance;
            _statusService = StatusService.Instance;
        }

        //[ChildActionOnly]
        public ActionResult UserLogged()
        {
            UserLoggedViewModel model = new UserLoggedViewModel();

            if (null != SessionHandler.User)
            {
                User user = _userService.GetProfile(SessionHandler.User.Id);

                model.Person = new PersonViewModel()
                {
                    Firstname = user.Person.Firstname,
                    Lastname = user.Person.Lastname
                };

                model.Username = user.Username;
                model.PictureUrl = null == user.Picture ? String.Empty : user.Picture.ThumbnailVirtualPath;
            }

            return PartialView("~/Areas/Admin/Views/Shared/_PartialLogged.cshtml", model);
        }

        //[ChildActionOnly]
        public ActionResult UsersList(string option = null)
        {
            IEnumerable<User> users = _userService.GetAllFull();

            IEnumerable<UserViewModel> model = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);

            ViewBag.Option = option;

            return PartialView("~/Areas/Admin/Views/User/_PartialUsersList.cshtml", model);
        }

        public ActionResult UserTypesList(string option = null)
        {
            IEnumerable<UserType> userTypes = _userTypeService.GetAll();

            IEnumerable<UserTypeViewModel> model = Mapper.Map<IEnumerable<UserType>, IEnumerable<UserTypeViewModel>>(userTypes);

            ViewBag.Option = option;

            return PartialView("~/Areas/Admin/Views/UserType/_PartialUserTypesList.cshtml", model);
        }

        public ActionResult UserType(int id, string option = null)
        {
            UserType userType = 0 < id ? _userTypeService.Get(id) : new UserType();

            UserTypeViewModel model = Mapper.Map<UserType, UserTypeViewModel>(userType);

            model = model ?? (model = new UserTypeViewModel() { Code = String.Empty });

            if ("0".Equals(model.Code)) model.Code = String.Empty;

            ViewBag.Option = option;

            return PartialView("~/Areas/Admin/Views/UserType/_PartialUserType.cshtml", model);
        }



        public ActionResult UserTypes(string id, string option)
        {
            UserTypePartialViewModel model = new UserTypePartialViewModel() { TypeId = id, Option = option };

            model.Types = Mapper.Map<IEnumerable<UserType>, IEnumerable<SelectListItem>>(_userTypeService.GetAll());

            return PartialView("~/Areas/Admin/Views/UserType/_PartialUserTypes.cshtml", model);
        }

        public ActionResult Statuses(string id, string option)
        {
            StatusPartialViewModel model = new StatusPartialViewModel() { StatusId = id, Option = option };

            model.Statuses = Mapper.Map<IEnumerable<StatusEntity>, IEnumerable<SelectListItem>>(_statusService.GetAll());

            return PartialView("~/Areas/Admin/Views/Shared/_PartialStatuses.cshtml", model);
        }
    }
}