﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Admin.Controllers
{
    [SessionTimeout]
    public partial class UserController : _AbstractController
    {
        private UserService _userService;
        private UserTypeService _userTypeService;
        private StatusService _statusService;

        public UserController()
        {
            _userService = UserService.Instance;
            _userTypeService = UserTypeService.Instance;
            _statusService = StatusService.Instance;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int? id)
        {
            User user = (id.HasValue && id.Value > 0 ? _userService.GetProfile(id.Value) : new User() { Person = new Person() });

            UserViewModel model = Mapper.Map<User, UserViewModel>(user);

            model = model ?? (model = new UserViewModel());

            return View(model);
        }
    }

    [SessionTimeout]
    public partial class UserController : _AbstractController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Detail(int? id, ProfileViewModel profile)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            User user = id.HasValue && id.Value > 0 ? _userService.GetWithPerson(id.Value) : new User();

            user.Username = profile.Username;
            user.Password = profile.Password;
            user.TypeId = Convert.ToInt32(profile.TypeId);
            user.ResolveSecurityFields(SessionHandler.User.Id);
            user.Status = Convert.ToInt16(profile.StatusId);

            Person person = user.Person ?? (user.Person = new Person());

            person.Firstname = profile.Person.Firstname;
            person.Lastname = profile.Person.Lastname;
            person.Email = profile.Person.Email;
            person.Email = profile.Person.Email;
            person.Gender = profile.Person.Gender;
            person.HomePhone = profile.Person.HomePhone;
            person.WorkPhone = profile.Person.WorkPhone;
            person.MobilPhone = profile.Person.MobilPhone;
            person.Birth = Convert.ToDateTime(profile.Person.Birth);
            person.Website = profile.Person.Website;
            person.Twitter = profile.Person.Twitter;
            person.Facebook = profile.Person.Facebook;
            person.GooglePlus = profile.Person.GooglePlus;
            person.Linkedin = profile.Person.Linkedin;
            person.Skype = profile.Person.Skype;
            person.ResolveSecurityFields(SessionHandler.User.Id);
            person.Status = Convert.ToInt16(profile.StatusId);

            _userService.Save(user);

            if(!id.HasValue || id.Value <= 0)
            {
                PGRMessage(responseMessageBuilder.Ok().Success().Message("Profile Info Save Successfully").Build());

                responseMessageBuilder.Clear();
                responseMessageBuilder.AddProperty("RedirectTo", Url.Action("Detail", "User", new { id = user.Id }));
            }
            else
            {
                responseMessageBuilder.Ok().Success().Message("Profile Info Save Successfully");
            }

            return Json(responseMessageBuilder.Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.Ok().Success()
                  .Message("Invalid user id").Build(), JsonRequestBehavior.AllowGet);
            }

            User user = _userService.Get(id);

            if(null == user || 0>= user.Id)
            {
                return Json(responseMessageBuilder.Ok().Success()
                  .Message("User not found").Build(), JsonRequestBehavior.AllowGet);
            }

            _userService.Delete(id);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("User deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }
    }
}