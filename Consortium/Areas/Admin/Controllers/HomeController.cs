﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.App_Code.Session;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Controllers;
using Consortium.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Admin.Controllers
{
    [SessionTimeout]
    public partial class HomeController : _AbstractController
    {
        private UserService _userService;
        private UserTypeService _userTypeService;
        private StatusService _statusService;

        public HomeController()
        {
            _userService = UserService.Instance;
            _userTypeService = UserTypeService.Instance;
            _statusService = StatusService.Instance;
        }

        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Profile()
        {
            User user = _userService.GetProfile(SessionHandler.User.Id);

            ProfileViewModel model = Mapper.Map<User, ProfileViewModel>(user);

            model = model ?? (model = new ProfileViewModel());

            return View(model);
        }

    }

    [SessionTimeout]
    public partial class HomeController : _AbstractController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Profile(ProfileViewModel profile)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            User user = _userService.GetProfile(SessionHandler.User.Id);

            Person person = user.Person;

            person.Firstname = profile.Person.Firstname;
            person.Email = profile.Person.Email;
            person.Email = profile.Person.Email;
            person.Gender = profile.Person.Gender;
            person.HomePhone = profile.Person.HomePhone;
            person.WorkPhone = profile.Person.WorkPhone;
            person.MobilPhone = profile.Person.MobilPhone;
            person.Birth = Convert.ToDateTime(profile.Person.Birth);
            person.Website = profile.Person.Website;
            person.Twitter = profile.Person.Twitter;
            person.Facebook = profile.Person.Facebook;
            person.GooglePlus = profile.Person.GooglePlus;
            person.Linkedin = profile.Person.Linkedin;
            person.Skype = profile.Person.Skype;

            //user = Mapper.Map<ProfileViewModel, User>(profile, user);

            _userService.SaveProfile(user);

            SessionHandler.User = user;

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Profle Info Save Successfully").Build(), JsonRequestBehavior.AllowGet);
        }

    }
}