﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Library.Common;
using Tier.Service;

namespace Consortium.Areas.Admin.Controllers
{
    [SessionTimeout]
    public partial class UserTypeController : _AbstractController
    {
        private UserTypeService _userTypeService;
        private StatusService _statusService;

        public UserTypeController()
        {
            _userTypeService = UserTypeService.Instance;
            _statusService = StatusService.Instance;
        }

        public ActionResult Index()
        {
            IEnumerable<UserType> userTypes = _userTypeService.GetAll();

            IEnumerable<UserTypeViewModel> model = Mapper.Map<IEnumerable<UserType>, IEnumerable<UserTypeViewModel>>(userTypes);

            model = model ?? (model = new List<UserTypeViewModel>());

            return View(model);
        }
    }

    [SessionTimeout]
    public partial class UserTypeController : _AbstractController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Detail(int? id, UserTypeViewModel model)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            UserType userType = id.HasValue && id.Value > 0 ? _userTypeService.Get(id.Value) : new UserType();

            userType.Code = Convert.ToInt32(model.Code);
            userType.Name = model.Name;
            userType.Description = model.Description;
            userType.Status = Convert.ToInt16(model.StatusId);
            userType.ResolveSecurityFields(SessionHandler.User.Id);


            _userTypeService.Save(userType);

            responseMessageBuilder.Ok().Success().Message("User type save successfully");

            return Json(responseMessageBuilder.Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.Ok().Success()
                  .Message("Invalid user type id").Build(), JsonRequestBehavior.AllowGet);
            }

            UserType userType = _userTypeService.Get(id);

            if (null == userType || 0 >= userType.Id)
            {
                return Json(responseMessageBuilder.Ok().Success()
                  .Message("User type not found").Build(), JsonRequestBehavior.AllowGet);
            }

            userType.Status = (short)EnumStatus.Deleted;
            userType.ResolveSecurityFields(SessionHandler.User.Id);

            _userTypeService.Delete(userType);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("User type deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }
    }
}