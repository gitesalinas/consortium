﻿using Consortium.App_Code.Base;
using Consortium.App_Code.Facades.FileUpload;
using Consortium.App_Code.Facades.FileUpload.Contracts;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using Consortium.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Library.Common;
using Tier.Library.Helper.File;
using Tier.Service;
using static Tier.Library.Helper.File.ImageFileHelper;

namespace Consortium.Areas.Admin.Controllers
{
    public class FileController : _AbstractController
    {
        private const string MODEL_IDENTIFIER_ROOM_MEDIA = "ROOM_MEDIA";
        private const string MODEL_IDENTIFIER_USER_PROFILE = "USER_PROFILE";
        private const string MODEL_IDENTIFIER_PRODUCT_LOGGING_MEDIA = "PRODUCT_LOGGING_MEDIA";
        private const string MODEL_IDENTIFIER_PRODUCT_FURNITURE_MEDIA = "PRODUCT_FURNITURE_MEDIA";

        private UserService _userService;
        private ResourceService _resourceService;

        public FileController()
        {
            _userService = UserService.Instance;
            _resourceService = ResourceService.Instance;
        }


        private JsonResult Upload(AbstractFileUploadManager fileUploadManager, ResponseMessageBuilder responseMessageBuilder)
        {
            fileUploadManager.Prepare();
            fileUploadManager.Save();

            return Json(responseMessageBuilder
                .Ok()
                .Success()
                .AddProperty("File", new
                {
                    Id = fileUploadManager.Id,
                    ThumbnailUrl = fileUploadManager.Resource.ThumbnailVirtualPath,
                    Url = fileUploadManager.Resource.VirtualPath
                })
                .Message("File uploaded successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        private AbstractFileUploadManager GetFileUploadManagerByIdentifier(FileUploadViewModel form, HttpPostedFileBase file)
        {
            switch (form.ModelIdentifier)
            {
                case MODEL_IDENTIFIER_ROOM_MEDIA:
                    return new RoomMediaFileUpload(SessionHandler.User.Id, form.OwnerId, new ImageFileHelper(file));
                case MODEL_IDENTIFIER_USER_PROFILE:
                    return new UserPictureFileUpload(form.OwnerId, new ImageFileHelper(file));
                case MODEL_IDENTIFIER_PRODUCT_LOGGING_MEDIA:
                    return new ProductLoggingMediaFileUpload(SessionHandler.User.Id, form.OwnerId, new ImageFileHelper(file));
                case MODEL_IDENTIFIER_PRODUCT_FURNITURE_MEDIA:
                    return new ProductFurnitureMediaFileUpload(SessionHandler.User.Id, form.OwnerId, new ImageFileHelper(file));
                default: throw new Exception("Invalid file upload identifier");
            }
        }


        //[HttpPost]
        //public JsonResult Upload(FileUploadViewModel form, HttpPostedFileBase file)
        //{
        //    ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

        //    if (null == form || 0 >= form.OwnerId)
        //    {
        //        return Json(responseMessageBuilder
        //            .NoOk()
        //            .Warning()
        //            .Message("Invalid owner id").Build(), JsonRequestBehavior.DenyGet);
        //    }

        //    ImageFileHelper _fileHelper = new ImageFileHelper(file);
        //    _fileHelper.CreatePaths();
        //    _fileHelper.SetThumbnailSize(EnumThumbnailSize.Large);
        //    _fileHelper.AppendUriToPaths("Room");

        //    RoomMediaFileUpload roomMediaFileUpload = new RoomMediaFileUpload(SessionHandler.User.Id, form.OwnerId, _fileHelper);

        //    roomMediaFileUpload.Prepare();
        //    roomMediaFileUpload.Save();

        //    return Json(responseMessageBuilder
        //        .Ok()
        //        .Success()
        //        .AddProperty("File", new
        //        {
        //            Id = roomMediaFileUpload.RoomMedia.Id,
        //            ThumbnailUrl = roomMediaFileUpload.Resource.ThumbnailVirtualPath,
        //            Url = roomMediaFileUpload.Resource.VirtualPath
        //        })
        //        .Message("File uploaded successfully").Build(), JsonRequestBehavior.AllowGet);
        //}


        //[HttpPost]
        //public JsonResult UploadRoomMedia(RoomMediaFileUploadViewModel form, HttpPostedFileBase file)
        //{
        //    ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

        //    if (null == form || 0 >= form.OwnerId)
        //    {
        //        return Json(responseMessageBuilder
        //            .NoOk()
        //            .Warning()
        //            .Message("Invalid owner id").Build(), JsonRequestBehavior.DenyGet);
        //    }

        //    //ImageFileHelper _fileHelper = new ImageFileHelper(file);
        //    //_fileHelper.CreatePaths();
        //    //_fileHelper.SetThumbnailSize(EnumThumbnailSize.Large);
        //    //_fileHelper.AppendUriToPaths("Room");

        //    //RoomMediaFileUpload roomMediaFileUpload = new RoomMediaFileUpload(SessionHandler.User.Id, form.OwnerId, _fileHelper);

        //    return Upload(GetFileUploadManagerByIdentifier(form, file), responseMessageBuilder);
        //}

        [HttpPost]
        public JsonResult Upload(FileUploadViewModel form, HttpPostedFileBase file)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if(null == form || 0 >= form.OwnerId)
            {
                return Json(responseMessageBuilder
                    .NoOk()
                    .Warning()
                    .Message("Invalid owner id").Build(), JsonRequestBehavior.DenyGet);
            }

            if (String.IsNullOrEmpty(form.ModelIdentifier))
            {
                return Json(responseMessageBuilder
                    .NoOk()
                    .Warning()
                    .Message("Invalid model identifier").Build(), JsonRequestBehavior.DenyGet);
            }

            //ImageFileHelper _fileHelper = new ImageFileHelper(file);
            //UserPictureFileUpload userPictureFileUpload = new UserPictureFileUpload(form.OwnerId, _fileHelper);

            return Upload(GetFileUploadManagerByIdentifier(form, file), responseMessageBuilder);

            //userPictureFileUpload.Prepare();
            //userPictureFileUpload.Save();

            //User user = _userService.Get(form.OwnerId);

            //if(null == user)
            //{
            //    return Json(responseMessageBuilder
            //        .NoOk()
            //        .Warning()
            //        .Message("User not found").Build(), JsonRequestBehavior.DenyGet);
            //}

            //Resource resource = (user.PictureId.HasValue ? _resourceService.Get(user.PictureId.Value) : new Resource());

            //if(null == resource) { resource = new Resource(); }

            //ImageFileHelper _fileHelper = new ImageFileHelper(picture, resource.PublicName);
            //_fileHelper.CreatePaths();
            //_fileHelper.AppendUriToPaths("User");

            //_fileHelper.SaveWithThumbnail();

            //resource.Name = _fileHelper.FileName;
            //resource.Extension = _fileHelper.FileName;
            //resource.ContentLength = _fileHelper.FileContentLength;
            //resource.ContentType = _fileHelper.FileContentType;

            //resource.PublicName = _fileHelper.FilePublicName;

            //resource.DirectoryPath = _fileHelper.FileDirectoryPath;
            //resource.Path = _fileHelper.FilePath;

            //resource.VirtualDirectoryPath = _fileHelper.FileVirtualDirectoryPath;
            //resource.VirtualPath = _fileHelper.FileVirtualPath;

            //resource.ThumbnailDirectoryPath = _fileHelper.FileThumbnailDirectoryPath;
            //resource.ThumbnailPath = _fileHelper.FileThumbnailPath;

            //resource.ThumbnailVirtualDirectoryPath = _fileHelper.FileThumbnailVirtualDirectoryPath;
            //resource.ThumbnailVirtualPath = _fileHelper.FileThumbnailVirtualPath;

            //resource.ResolveSecurityFields(SessionHandler.User.Id);
            //resource.SetEnabled();

            //_userService.SaveProfilePicture(user, resource);

            //return Json(responseMessageBuilder
            //    .Ok()
            //    .Success()
            //    .AddProperty("File", new { ThumbnailUrl = userPictureFileUpload.Resource.ThumbnailVirtualPath, Url = userPictureFileUpload.Resource.VirtualPath })
            //    .Message("Image Uploaded Successfully").Build(), JsonRequestBehavior.DenyGet);
        }
    }
}