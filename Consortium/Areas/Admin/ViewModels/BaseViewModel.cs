﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Admin.ViewModels
{
    public class BaseViewModel
    {
        public int Id { get; set; }

        public bool HasId { get { return 0 < Id; } }
    }
}