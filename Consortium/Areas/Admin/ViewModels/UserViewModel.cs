﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Admin.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Username is a required field")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is a required field")]
        public string Password { get; set; }
        public string TypeId { get; set; }
        public string Type { get; set; }

        private string _pictureUrl;
        public string PictureUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_pictureUrl)) return String.Empty;
                return $"{_pictureUrl}?t={(long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds}";
            }
            set
            {
                _pictureUrl = value;
            }
        }

        public string DefaultPictureUrl { get { return VirtualPathUtility.ToAbsolute("~/Content/assets/images/avatar-1-xl.jpg"); } }

        public PersonViewModel Person { get; set; }

        public string StatusId { get; set; }
        public string Status { get; set; }

        public bool HasId { get { return !String.IsNullOrEmpty(Id) && !"0".Equals(Id); } }
    }
}