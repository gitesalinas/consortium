﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Admin.ViewModels
{
    public class ResourceViewModel
    {
        public string Name { get; set; }
        public string VirtualPath { get; set; }
        public string ThumbnailVirtualPath { get; set; }

        public short StatusId { get; set; }
        public string Status { get; set; }
    }
}