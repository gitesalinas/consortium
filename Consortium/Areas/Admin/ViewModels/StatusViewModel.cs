﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Admin.ViewModels
{
    public class StatusViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StatusId { get; set; }
        public string Status { get; set; }
    }

    public class StatusPartialViewModel
    {
        public string StatusId { get; set; }
        public string Option { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }
    }
}