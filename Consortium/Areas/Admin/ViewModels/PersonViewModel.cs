﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Admin.ViewModels
{
    public class PersonViewModel
    {
        public string FullName { get { return $"{Firstname} {Lastname}"; } }

        [Required(ErrorMessage = "Firstname is a required")]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Lastname is a required")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Email is a required")]
        [EmailAddress(ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilPhone { get; set; }


        [Required(ErrorMessage = "Bird is a required")]
        public string Birth { get; set; }
        [Required(ErrorMessage = "Gender is a required")]
        public string _gender;
        public string Gender
        {
            get
            {
                return String.IsNullOrEmpty(_gender) ? "M" : _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public string Website { get; set; }

        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string Linkedin { get; set; }
        public string Skype { get; set; }
    }
}