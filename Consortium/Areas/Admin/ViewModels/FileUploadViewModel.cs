﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Admin.ViewModels
{
    public class FileUploadViewModel
    {
        //[Required(ErrorMessage = "Id is a required field")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Owner Id is a required field")]
        public int OwnerId { get; set; }
        [Required(ErrorMessage = "Model identifier is a required field")]
        public string ModelIdentifier { get; set; }
        //[Required(ErrorMessage = "Please choose a file to upload")]
        //public HttpPostedFileBase File { get; set; }
    }
}