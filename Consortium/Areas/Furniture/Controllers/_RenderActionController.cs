﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Furniture.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using Consortium.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Furniture.Controllers
{
    [SessionTimeout]
    public partial class _RenderActionController : _AbstractController
    {
        private ProductFurnitureService _productService { get { return ProductFurnitureService.Instance; } }
        private ProductFurnitureMediaService _productMediaService { get { return ProductFurnitureMediaService.Instance; } }
        private ProductTypeService _productTypeService { get { return ProductTypeService.Instance; } }

        private ProductFurnitureRelationshipService _productRelationshipService { get { return ProductFurnitureRelationshipService.Instance; } }

        public ActionResult Products(string option = null)
        {
            IEnumerable<ProductFurniture> products = _productService.GetAll();

            IEnumerable<ProductViewModel> model = Mapper.Map<IEnumerable<ProductFurniture>, IEnumerable<ProductViewModel>>(products);

            ViewBag.Option = option;

            return PartialView("~/Areas/Furniture/Views/Product/_PartialProducts.cshtml", model);
        }

        public ActionResult ProductTypes(int id, string option)
        {
            ProductTypesViewModel model = new ProductTypesViewModel() { TypeId = id, Option = option };

            model.Types = Mapper.Map<IEnumerable<ProductType>, IEnumerable<SelectListItem>>(_productTypeService.GetAll());

            return PartialView("~/Areas/Furniture/Views/Shared/_PartialProductTypes.cshtml", model);
        }

        public ActionResult ProductMedias(int id, string option)
        {
            ProductMediasViewModel model = new ProductMediasViewModel() { Option = option };

            IEnumerable<ProductFurnitureMedia> productMedias = _productMediaService.GetAllByProductId(id);
            productMedias = productMedias ?? new List<ProductFurnitureMedia>();
            model.ProductMedias = Mapper.Map<IEnumerable<ProductFurnitureMedia>, IEnumerable<ProductMediaViewModel>>(productMedias);

            return PartialView("~/Areas/Furniture/Views/Product/_PartialProductMedias.cshtml", model);
        }


        public ActionResult ProductsAvailableForRelationship(int id, string option, string attributes = null)
        {
            WrapperProductsRelationshipAvailableViewModel model = new WrapperProductsRelationshipAvailableViewModel()
            {
                Option = option,
                Attributes = ResolveAttributes(attributes)
            };

            //ProductsViewModel model = new ProductsViewModel() { Option = option, Attributes = ResolveAttributes(attributes) };

            IEnumerable<ProductFurniture> products = _productService.GetAllForRelationship(id);
            products = products ?? new List<ProductFurniture>();

            if ("select".Equals(option))
            {
                model.DropDownList = Mapper.Map<IEnumerable<ProductFurniture>, IEnumerable<SelectListItem>>(products);
            }
            else
            {
                model.ProductsAvailableForRelationship = Mapper.Map<IEnumerable<ProductFurniture>, IEnumerable<ProductViewModel>>(products);
            }

            return PartialView("~/Areas/Furniture/Views/Product/_PartialProductsAvailableForRelationship.cshtml", model);
        }

        public ActionResult ProductsInRelationship(int id, string option, string attributes = null)
        {
            WrapperProductsRelationshipViewModel model = new WrapperProductsRelationshipViewModel() { Option = option, Attributes = ResolveAttributes(attributes) };

            IEnumerable<ProductFurnitureRelationship> products = _productRelationshipService.GetAllByProductOwner(id);
            products = products ?? new List<ProductFurnitureRelationship>();

            model.Items = Mapper.Map<IEnumerable<ProductFurnitureRelationship>, IEnumerable<ProductRelationshipViewModel>>(products);

            return PartialView("~/Areas/Furniture/Views/Product/_PartialProductsInRelationship.cshtml", model);
        }



        private Dictionary<string, object> ResolveAttributes(string attrs)
        {
            Dictionary<string, object> attributes = new Dictionary<string, object>();

            if (String.IsNullOrEmpty(attrs)) return attributes;

            attributes = attrs
                .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(part => part.Split('='))
                .ToDictionary(split => split[0], split => (object)split[1]);

            return attributes;
        }
    }
}