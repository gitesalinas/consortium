﻿using AutoMapper;
using Consortium.App_Code.Base;
using Consortium.App_Code.Filters;
using Consortium.Areas.Furniture.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Areas.Furniture.Controllers
{
    [SessionTimeout]
    public partial class ProductController : _AbstractController
    {
        private ProductFurnitureService _productService { get { return ProductFurnitureService.Instance; } }
        private ProductFurnitureMediaService _productMediaService { get { return ProductFurnitureMediaService.Instance; } }
        private ProductFurnitureRelationshipService _productRelationshipService { get { return ProductFurnitureRelationshipService.Instance; } }

        // GET: Furniture/Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int? id)
        {
            ProductFurniture product = id.HasValue && 0 < id.Value ? _productService.Get(id.Value) : new ProductFurniture();

            ProductViewModel model = Mapper.Map<ProductFurniture, ProductViewModel>(product);
            model = model ?? new ProductViewModel();

            if (id.HasValue)
            {
                ProductFurnitureMedia productMedia = _productMediaService.GetMainProductMedia(id.Value);
                if (null != productMedia && 0 < productMedia.Id)
                {
                    model.ImageUrl = productMedia.Resource.ThumbnailVirtualPath;
                }
            }

            return View(model);
        }
    }

    [SessionTimeout]
    public partial class ProductController : _AbstractController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Detail(int? id, ProductViewModel model)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurniture product = id.HasValue && 0 < id.Value ? _productService.Get(id.Value) : new ProductFurniture();

            product.SKU = model.SKU;
            product.Name = model.Name;
            product.Description = model.Description;
            product.FullDescription = model.FullDescription;
            product.Stock = Convert.ToDecimal(model.Stock);
            product.Tax = Convert.ToDecimal(model.Tax);
            product.UnitPrice = Convert.ToDecimal(model.UnitPrice);
            product.TotalPrice = Convert.ToDecimal(model.TotalPrice);
            product.TypeId = Convert.ToInt32(model.TypeId);
            product.Status = Convert.ToInt16(model.StatusId);
            product.ResolveSecurityFields(SessionHandler.User.Id);

            _productService.Save(product);

            if (!id.HasValue || id.Value <= 0)
            {
                PGRMessage(responseMessageBuilder.Ok().Success().Message("Product info save successfully").Build());

                responseMessageBuilder.Clear();
                responseMessageBuilder.AddProperty("RedirectTo", Url.Action("Detail", "Product", new { id = product.Id }));
            }
            else
            {
                responseMessageBuilder.Ok().Success().Message("Product info save successfully");
            }

            return Json(responseMessageBuilder.Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product id").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurniture product = _productService.Get(id);

            if (null == product || 0 >= product.Id)
            {
                return Json(responseMessageBuilder.NoOk().Info()
                  .Message("Product not found").Build(), JsonRequestBehavior.AllowGet);
            }

            product.ResolveSecurityFields(SessionHandler.User.Id);
            product.SetDeleted();

            _productService.Delete(product);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Product deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult HandleMainProductMedia(int id, int productMediaId)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product id").Build(), JsonRequestBehavior.AllowGet);
            }

            if (0 >= productMediaId)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product media id").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurnitureMedia productMedia = _productMediaService.Get(productMediaId);

            if (null == productMedia || 0 >= productMedia.Id)
            {
                return Json(responseMessageBuilder.NoOk().Info()
                  .Message("Product media not found").Build(), JsonRequestBehavior.AllowGet);
            }

            productMedia.ProductId = id;
            productMedia.ResolveSecurityFields(SessionHandler.User.Id);

            _productMediaService.HandleMain(productMedia);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Product media main setted successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadProductForRelationship(int productOwnerId, int productDependantId)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= productOwnerId)
            {
                ResponseError("Invalid product owner id");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product owner id").Build(), JsonRequestBehavior.AllowGet);
            }

            if (0 >= productDependantId)
            {
                ResponseError("Invalid product dependant id");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product dependant id").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurniture product = _productService.Get(productDependantId);

            if (null == product || 0 >= product.Id)
            {
                ResponseError("Product not found");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Product not found").Build(), JsonRequestBehavior.AllowGet);
            }

            return Json(responseMessageBuilder.Ok().Success().Message("Product loaded successfully")
                .AddProperty("Response", new {
                    Id = 0,
                    ProductDependantId = product.Id,
                    SKU = product.SKU,
                    Name = product.Name,
                    Tax = product.Tax,
                    UnitPrice = product.UnitPrice,
                    Units = 1,
                    TotalPrice = product.TotalPrice
                })
                .Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProductRelationship(ProductRelationshipViewModel model)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= model.ProductOwnerId)
            {
                ResponseError("Invalid product owner id");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product owner id").Build(), JsonRequestBehavior.AllowGet);
            }

            if (0 >= model.ProductDependantId)
            {
                ResponseError("Invalid product dependant id");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product dependant id").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurniture product = _productService.Get(model.ProductDependantId);

            if (null == product || 0 >= product.Id)
            {
                ResponseError("Product dependant not found");
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Product dependant not found").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurnitureRelationship productRelationship = null;

            if (model.HasId)
            {
                productRelationship = _productRelationshipService.Get(model.Id);
            }
            else
            {
                productRelationship = _productRelationshipService
                    .GetByProductOwnerAndProductDependant(model.ProductOwnerId, model.ProductDependantId);
            }

            productRelationship = productRelationship ?? new ProductFurnitureRelationship();

            productRelationship.SKU = product.SKU;
            productRelationship.Name = model.Name;
            productRelationship.ProductOwnerId = model.ProductOwnerId;
            productRelationship.ProductDependantId = model.ProductDependantId;
            productRelationship.Units = model.Units;
            productRelationship.Tax = model.Tax;
            productRelationship.UnitPrice = model.UnitPrice;
            productRelationship.TotalPrice = model.TotalPrice;
            productRelationship.SetEnabled();
            productRelationship.ResolveSecurityFields(SessionHandler.User.Id);

            _productRelationshipService.Save(productRelationship);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Product realtionship saved successfully").Build(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveProductRelationship(int id, int productRelationshipId)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (0 >= id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product id").Build(), JsonRequestBehavior.AllowGet);
            }

            if (0 >= productRelationshipId)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Invalid product relationship id").Build(), JsonRequestBehavior.AllowGet);
            }

            ProductFurnitureRelationship productRelationship = _productRelationshipService.Get(productRelationshipId);

            if (null == productRelationship || 0 >= productRelationship.Id)
            {
                return Json(responseMessageBuilder.NoOk().Warning()
                  .Message("Product relationship not found").Build(), JsonRequestBehavior.AllowGet);
            }

            productRelationship.ResolveSecurityFields(SessionHandler.User.Id);
            productRelationship.SetDeleted();

            _productRelationshipService.Delete(productRelationship);

            return Json(responseMessageBuilder.Ok().Success()
                .Message("Product relationship deleted successfully").Build(), JsonRequestBehavior.AllowGet);
        }
    }
}