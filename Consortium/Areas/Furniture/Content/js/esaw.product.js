﻿; (function ($, window, document, undefined, util, ajaxAware) {
    var params = {};

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            if (params.productInfoForm) {
                params.productInfoForm.$.validate(params.productInfoFormValidations);

                if (params.productMediaColorbox) params.productMediaColorbox.init();
                if (params.productMediaDropzone) params.productMediaDropzone.init();
            }

            if (params.productsTable)
                params.productsTable.init();
        },
        _saveProductInfo: function ($ele) {

            ajaxAware.subscribe($ele[0], params.productInfoForm.ajaxLoader);

            var ajax = util.ajaxCall(params.productInfoForm.action, params.productInfoForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                if (typeof data.RedirectTo !== "undefined") {
                    window.location.href = data.RedirectTo;
                    return;
                }

                params.productInfoForm.messenger.showMessage(data.Messages[0]);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.productInfoForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });

            return false;
        },
        _delete: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteProductForm.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteProductForm.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteProductForm.messenger.showMessage(data.Messages[0]);
                params.deleteModal.hide();

                that._load(params.loadProductsForm.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteProductForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _load: function ($ele) {

            params.productsTable.destroy();

            params.productsTable.clearBody();

            ajaxAware.subscribe($ele[0], params.loadProductsTable.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadProductsTable.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadProductsTable.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteUserForm.messenger);
            }).always(function (data, textStatus, jqXHR) { params.productsTable.init(); ajaxAware.unsubscribe($ele[0]); });
        },
        _deleteProductMedia: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteProductMediaGrid.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteProductMediaGrid.action, { id: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteProductMediaGrid.messenger.showMessage(data.Messages[0]);
                params.deleteProductMediaModal.hide();

                that._loadProductMedias(params.loadProductMedias.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteProductMediaGrid.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _loadProductMedias: function ($ele) {

            ajaxAware.subscribe($ele[0], params.loadProductMedias.ajaxLoader);

            var ajax = util.ajaxCallContent(params.loadProductMedias.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.loadProductMedias.$.html(data);

                if (params.loadProductMedias.callback) params.loadProductMedias.callback();

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadProductMedias.messenger);
            }).always(function (data, textStatus, jqXHR) { params.productMediaColorbox.init(); ajaxAware.unsubscribe($ele[0]); });
        },
        _handleMainProductMedia: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.handleMainProductMedia.ajaxLoader);

            var ajax = util.ajaxCall(params.handleMainProductMedia.action, { productMediaId: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.handleMainProductMedia.messenger.showMessage(data.Messages[0]);

                that._loadProductMedias(params.loadProductMedias.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadProductMedias.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },

        _loadProductForRelationship: function ($ele, id) {
            ajaxAware.subscribe($ele[0], params.productRelationshipLoadForm.ajaxLoader);

            var ajax = util.ajaxCall(params.productRelationshipLoadForm.action, { productDependantId: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.productRelationshipLoadForm.bindModel(data.Response);

                if (params.productRelationshipLoadForm.extra
                    && params.productRelationshipLoadForm.extra.makeFormVisible) params.productRelationshipLoadForm.extra.makeFormVisible(true);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                params.productRelationshipLoadForm.extra.makeFormVisible(false);
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loadProductMedias.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },


        _addProductRelationship: function ($ele) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.productRelationshipForm.ajaxLoader);

            var ajax = util.ajaxCall(params.productRelationshipForm.action, params.productRelationshipForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                params.productRelationshipLoadForm.clearForm();
                params.productRelationshipLoadForm.extra.makeFormVisible(false);

                params.productRelationshipForm.messenger.showMessage(data.Messages[0]);

                that._loadProductRelationships(params.productRelationshipLoadTable.$);
                that._loadProductAvailableForRelationship(params.productAvailableForRelationshipLoad.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.productRelationshipForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });

            return false;
        },
        _loadProductAvailableForRelationship: function ($ele) {
            ajaxAware.subscribe($ele[0], params.productAvailableForRelationshipLoad.ajaxLoader);

            var ajax = util.ajaxCallContent(params.productAvailableForRelationshipLoad.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.productAvailableForRelationshipLoad.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.productAvailableForRelationshipLoad.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },
        _loadProductRelationships: function ($ele) {
            ajaxAware.subscribe($ele[0], params.productRelationshipLoadTable.ajaxLoader);

            var ajax = util.ajaxCallContent(params.productRelationshipLoadTable.action);

            ajax.done(function (data, textStatus, jqXHR) {

                params.productRelationshipLoadTable.$.html(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.productRelationshipLoadTable.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        },

        _deleteProductRelationship: function ($ele, id) {
            var that = this;

            ajaxAware.subscribe($ele[0], params.deleteProductRelationshipTable.ajaxLoader);

            var ajax = util.ajaxCall(params.deleteProductRelationshipTable.action, { productRelationshipId: id });

            ajax.done(function (data, textStatus, jqXHR) {

                params.deleteProductRelationshipTable.messenger.showMessage(data.Messages[0]);
                params.deleteProductRelationshipModal.hide();

                that._loadProductRelationships(params.productRelationshipLoadTable.$);
                that._loadProductAvailableForRelationship(params.productAvailableForRelationshipLoad.$);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.deleteProductRelationshipTable.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele[0]); });
        }

    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        saveProductInfo: function (ele) {

            if (!params.productInfoForm.$.valid()) {
                return;
            }

            return functions._saveProductInfo($(ele));
        },
        confirmDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteModal.setHandlers("$.fn.productJS('delete', this, " + id + ")", "");
            params.deleteModal.show();
        },
        delete: function (ele, id) {

            if (!isId(id)) return;

            functions._delete($(ele), id);
        },
        uploadAllMediaFiles: function (ele) {
            params.productMediaDropzone.uploadAllFiles();
        },
        confirmProductMediaDelete: function (ele, id) {

            if (!isId(id)) return;

            params.deleteProductMediaModal.setHandlers("$.fn.productJS('deleteProductMedia', this, " + id + ")", "");
            params.deleteProductMediaModal.show();
        },
        deleteProductMedia: function (ele, id) {
            if (!isId(id)) return;

            functions._deleteProductMedia($(ele), id);
        },
        loadProductMedias: function (ele) {
            functions._loadProductMedias($(ele));
        },
        handleMainProductMedia: function (ele, id) {
            if (!isId(id)) return;

            functions._handleMainProductMedia($(ele), id);
        },

        closeProductRelationship: function (ele) {

            params.productRelationshipLoadForm.clearForm();
            params.productRelationshipLoadForm.extra.makeFormVisible(false);

            return false;
        },

        loadProductForRelationship: function (ele, id) {

            params.productRelationshipLoadForm.clearForm();

            if (!isId(id)) {
                params.productRelationshipLoadForm.extra.makeFormVisible(false);
                return;
            }

            functions._loadProductForRelationship($(ele), id);
        },

        addProductRelationship: function (ele) {
            return functions._addProductRelationship($(ele));
        },

        confirmDeleteProductRelationship: function (ele, id) {
            if (!isId(id)) return;

            params.deleteProductRelationshipModal.setHandlers("$.fn.productJS('deleteProductRelationship', this, " + id + ")", "");
            params.deleteProductRelationshipModal.show();
        },
        deleteProductRelationship: function (ele, id) {
            if (!isId(id)) return;

            functions._deleteProductRelationship($(ele), id);
        }
    };

    $.fn.productJS = function (parameter) {
        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.productJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWAjaxAware);