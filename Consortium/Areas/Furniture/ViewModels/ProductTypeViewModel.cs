﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Furniture.ViewModels
{
    public class ProductTypeViewModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StatusId { get; set; }
        public string Status { get; set; }
    }

    public class ProductTypesViewModel
    {
        public int TypeId { get; set; }
        public string Option { get; set; }
        public IEnumerable<SelectListItem> Types { get; set; }
    }
}