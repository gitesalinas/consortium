﻿using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consortium.Areas.Furniture.ViewModels
{
    public class ProductMediaViewModel : BaseViewModel
    {
        //public string Text { get; set; }
        public bool IsMain { get; set; }

        public int ProductId { get; set; }
        public ProductViewModel Product { get; set; }

        public int ResourceId { get; set; }
        public ResourceViewModel Resource { get; set; }

        public short StatusId { get; set; }
        public string Status { get; set; }
    }

    public class ProductMediasViewModel
    {
        public string Option { get; set; }
        private IEnumerable<ProductMediaViewModel> _productMedias;
        public IEnumerable<ProductMediaViewModel> ProductMedias {
            get
            {
                return _productMedias ?? (_productMedias = new List<ProductMediaViewModel>());
            }
            set
            {
                _productMedias = value;
            }
        }
    }
}