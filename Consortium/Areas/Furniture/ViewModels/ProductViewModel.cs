﻿using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Furniture.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }

        public decimal Stock { get; set; }

        public decimal Tax { get; set; }
        public decimal UnitPrice { get; set; }

        public decimal TotalPrice { get; set; }

        public string Type { get; set; }
        public int TypeId { get; set; }

        public string StatusId { get; set; }
        public string Status { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_imageUrl)) return DefaultImageUrl;
                return $"{_imageUrl}?t={(long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds}";
            }
            set
            {
                _imageUrl = value;
            }
        }

        public string DefaultImageUrl { get { return VirtualPathUtility.ToAbsolute("~/Content/assets/images/default-image.png"); } }

        public List<ProductMediaViewModel> ProductMedias = new List<ProductMediaViewModel>();
    }


    public class ProductsViewModel
    {
        public string Option { get; set; }
        private IEnumerable<ProductViewModel> _products;
        public IEnumerable<ProductViewModel> Products
        {
            get
            {
                return _products ?? (_products = new List<ProductViewModel>());
            }
            set
            {
                _products = value;
            }
        }

        public Dictionary<string, object> Attributes { get; set; }
        public IEnumerable<SelectListItem> DropDownList { get; set; }
    }


    public class WrapperProductsRelationshipAvailableViewModel
    {
        public string Option { get; set; }
        private IEnumerable<ProductViewModel> _productsAvailableForRelationship;
        public IEnumerable<ProductViewModel> ProductsAvailableForRelationship
        {
            get
            {
                return _productsAvailableForRelationship ?? (_productsAvailableForRelationship = new List<ProductViewModel>());
            }
            set
            {
                _productsAvailableForRelationship = value;
            }
        }

        public Dictionary<string, object> Attributes { get; set; }
        public IEnumerable<SelectListItem> DropDownList { get; set; }
    }





}