﻿using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consortium.Areas.Furniture.ViewModels
{
    public class ProductRelationshipViewModel : BaseViewModel
    {
        public int ProductOwnerId { get; set; }

        public int ProductDependantId { get; set; }

        public string SKU { get; set; }
        public string Name { get; set; }

        public decimal Units { get; set; }

        public decimal Tax { get; set; }
        public decimal UnitPrice { get; set; }

        public decimal TotalPrice { get; set; }

        public string StatusId { get; set; }
        public string Status { get; set; }
    }

    public class WrapperProductRelationshipViewModel
    {

    }

    public class WrapperProductsRelationshipViewModel
    {
        public string Option { get; set; }
        private IEnumerable<ProductRelationshipViewModel> _items;
        public IEnumerable<ProductRelationshipViewModel> Items
        {
            get
            {
                return _items ?? (_items = new List<ProductRelationshipViewModel>());
            }
            set
            {
                _items = value;
            }
        }

        private Dictionary<string, object> _attributes;
        public Dictionary<string, object> Attributes {
            get
            {
                return _attributes ?? (_attributes = new Dictionary<string, object>());
            }
            set
            {
                _attributes = value;
            }
        }

        public IEnumerable<SelectListItem> DropDownList { get; set; }
    }
}