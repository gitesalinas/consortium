﻿using Consortium.App_Code.Base;
using Consortium.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;
using Tier.Service;

namespace Consortium.Controllers
{
    public partial class HomeController : _AbstractController
    {
        private UserService UserService { get { return UserService.Instance; } }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Account(string returnUrl)
        {
            ViewBag.LoginUrl = String.IsNullOrEmpty(returnUrl) ?
                Url.Action("Login", "Home") : Url.Action("Login", "Home", new { returnUrl = returnUrl });

            ViewBag.RegisterUrl = Url.Action("Register", "Home");

            return View();
        }

        public ActionResult Logout()
        {
            SessionHandler.Logout();

            return RedirectToAction("Account", "Home", new { Area = String.Empty });
        }
    }

    public partial class HomeController : _AbstractController
    {
        public JsonResult Login(LoginViewModel login, string returnUrl)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk().Warning()
                    .ModelStateErrorsString(ModelState)
                    //.Message("Username or password invalid")
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            try
            {
                User user = UserService.Login(login.Username, login.Password);

                if (user != null)
                {
                    SessionHandler.User = user;

                    string url = String.IsNullOrEmpty(returnUrl) ?
                        Url.Action("Dashboard", "Home", new { Area = "Admin" }) : returnUrl;

                    return Json(responseMessageBuilder.Ok().Success().Message("User successfully logged")
                        .AddProperty("RedirectTo", url).Build(), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ResponseError(ex.Message);
                return Json(responseMessageBuilder.NoOk().Error()
                    .Message(ex.Message).Build(), JsonRequestBehavior.AllowGet);
            }

            ResponseError("User Not Found");
            return Json(responseMessageBuilder.NoOk().Info()
                .Message("User Not Found").Build(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Register(RegisterViewModel register)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder
                    .NoOk()
                    .ModelStateErrorsString(ModelState)
                    .Build(), JsonRequestBehavior.AllowGet);
            }

            //User user = Mapper.Map<RegisterViewModel, User>(register);

            //user.Person.SetEnabled();
            //user.Person.ResolveSecurityFields();
            //user.SetEnabled();
            //user.ResolveSecurityFields();

            try
            {
                //SessionHandler.User = user;
                //user = UserService.Register(user);

                //if (user != null)
                //{
                //    return Json(responseMessageBuilder.Ok().Success().Message("User successfully registered")
                //        .AddProperty("RedirectTo", Url.Action("Dashboard", "Admin")).Build(), JsonRequestBehavior.AllowGet);
                //}
            }
            catch (Exception ex)
            {
                ResponseError(ex.Message);
                return Json(responseMessageBuilder
                    .NoOk()
                    .Error()
                    .Message(ex.Message).Build(), JsonRequestBehavior.AllowGet);
            }

            ResponseError("User Not Found");
            return Json(responseMessageBuilder
                .NoOk()
                .Info()
                .Message("User Not Found").Build(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ForgotPassword([EmailAddress(ErrorMessage = "Invalid Email Format")]string email)
        {
            ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();

            if (!ModelState.IsValid)
            {
                ResponseError("Invalid ModelState");
                return Json(responseMessageBuilder.NoOk()
                    .ModelStateErrorsString(ModelState).Build(), JsonRequestBehavior.AllowGet);
            }

            try
            {

            }
            catch (Exception ex)
            {
                ResponseError(ex.Message);
                return Json(responseMessageBuilder.NoOk().Error().Message(ex.Message).Build(), JsonRequestBehavior.AllowGet);
            }

            ResponseError("User Not Found");
            return Json(responseMessageBuilder.NoOk().Info().Message("User Not Found").Build(), JsonRequestBehavior.AllowGet);
        }
    }
}
