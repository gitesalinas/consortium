﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tier.Entity;
using Tier.Library.Common;
using Tier.Service;

namespace Consortium.Controllers
{
    public class ValuesController : ApiController
    {
        public User GetUser()
        {
            User user = UserService.Instance.LoginAPI("esalinas", "123456");
            return user;
        }

        public User GetUser2()
        {
            string param = Request.Headers.GetValues("eder").FirstOrDefault();
            string authorization = Request.Headers.Authorization.Parameter;

            return new User() { Username = "esalinas", Password = "123456", Status = (short)EnumStatus.Enabled };
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
