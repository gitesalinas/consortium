﻿using AutoMapper;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;

namespace Consortium.Mapping
{
    public class ViewModelToModel : Profile
    {
        public override string ProfileName => base.ProfileName;

        public ViewModelToModel()
        {
            CreateMap<UserTypeViewModel, UserType>();

            CreateMap<RoomViewModel, Room>();

            CreateMap<PersonViewModel, Person>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<ProfileViewModel, User>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<UserViewModel, User>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}