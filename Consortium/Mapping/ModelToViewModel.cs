﻿using AutoMapper;
using Consortium.Areas.Admin.ViewModels;
using Consortium.Areas.Furniture.ViewModels;
using Consortium.Areas.Hotel.ViewModels;
using Consortium.Mapping.Resolver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Common;

namespace Consortium.Mapping
{
    public class ModelToViewModel : Profile
    {
        public override string ProfileName => base.ProfileName;

        public ModelToViewModel()
        {
            CreateMap<StatusEntity, StatusViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<StatusViewModel>>());

            CreateMap<Room, RoomViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<RoomViewModel>>());

            CreateMap<UserType, UserTypeViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<UserTypeViewModel>>());

            CreateMap<Person, PersonViewModel>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<User, ProfileViewModel>()
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProfileViewModel>>())
                .ForMember(dest => dest.PictureUrl, opt => opt.MapFrom(src => src.Picture.ThumbnailVirtualPath))
                //.ForMember(dest => dest.Id, opt => opt.Condition(source => source.Id > 0))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.TypeId))
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<UserViewModel>>())
                .ForMember(dest => dest.PictureUrl, opt => opt.MapFrom(src => src.Picture.ThumbnailVirtualPath))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.Name))
                //.ForMember(dest => dest.Id, opt => opt.Condition(source => source.Id > 0))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));


            CreateMap<Person, PersonViewModel>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<Resource, ResourceViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ResourceViewModel>>());

            CreateMap<RoomMedia, RoomMediaViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<RoomMediaViewModel>>());

            CreateMap<ProductFurniture, ProductViewModel>()
                .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.Type.Id))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.Name))
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductViewModel>>());

            CreateMap<ProductLogging, ProductViewModel>()
                .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.Type.Id))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.Name))
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductViewModel>>());

            CreateMap<ProductFurnitureMedia, ProductMediaViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductMediaViewModel>>());

            CreateMap<ProductLoggingMedia, ProductMediaViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductMediaViewModel>>());

            CreateMap<ProductType, ProductTypeViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductTypeViewModel>>());

            CreateMap<ProductFurnitureRelationship, ProductRelationshipViewModel>()
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Status, opt => opt.ResolveUsing<StatusToStringConverter<ProductRelationshipViewModel>>());
        }
    }
}