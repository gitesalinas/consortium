﻿using AutoMapper;
using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tier.Entity;
using Tier.Library.Common;

namespace Consortium.Mapping.Resolver
{
    public class StatusToStringConverter<T> : IValueResolver<BaseEntity, T, string> where T : new()
    {
        //public string Resolve(BaseEntity source, UserTypeViewModel destination, string destMember, ResolutionContext context)
        //{
        //    return ((EnumStatus)source.Status).GetStringValue();
        //}

        public string Resolve(BaseEntity source, T destination, string destMember, ResolutionContext context)
        {
            return ((EnumStatus)source.Status).GetStringValue();
        }
    }
}