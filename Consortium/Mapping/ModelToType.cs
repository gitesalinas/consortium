﻿using AutoMapper;
using Consortium.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tier.Entity;

namespace Consortium.Mapping
{
    public class ModelToType : Profile
    {
        public override string ProfileName => base.ProfileName;

        public ModelToType()
        {
            CreateMap<Facility, SelectListItem>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));

            CreateMap<UserType, SelectListItem > ()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));

            CreateMap<StatusEntity, SelectListItem>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));

            CreateMap<ProductType, SelectListItem>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));


            CreateMap<ProductFurniture, SelectListItem>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));
        }
    }
}