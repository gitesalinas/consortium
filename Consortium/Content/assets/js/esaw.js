﻿var ESAWUtilitiesJS = {
    //showModelStateErrorMessages: function ($wrapper, modelState, validator) {

    //    //if ("jquery.validate" == validator) {
    //    $wrapper.validate().resetForm();

    //    for (var i = 0; i < modelState.length; i++) {
    //        $("*[name='" + modelState[i].Field + "']", $wrapper).valid();
    //        //for (var j = 0; j < modelState.Errors; j++) {  }
    //    }
    //    //}
    //},
    handleAjaxResponseError: function (jqXHR, textStatus, errorThrown, messenger) {
        if (errorThrown != 'abort' && jqXHR.status != 0) {
            if (jqXHR.status == ESAWConsts.ajaxErrorCode) {

                //if (jqXHR.responseJSON.ModelStateMessages && jqXHR.responseJSON.ModelStateMessages.length > 0)
                    //ESAWUtilitiesJS.showModelStateErrorMessages(jqXHR.$modelStateWrapper, jqXHR.responseJSON.ModelStateMessages);

                if (jqXHR.responseJSON.Messages && jqXHR.responseJSON.Messages.length > 0)
                    messenger.showMessage(jqXHR.responseJSON.Messages[0]);
            }
            else if (jqXHR.status == ESAWConsts.ajaxSessionTimeoutCode) {
                location.href = jqXHR.responseJSON.RedirectTo;
            }
            else { alert(textStatus); }
        }
    },
    scrollTo: function (time, position) {
        jQuery("html, body").animate({ scrollTop: position }, time);
    },
    ajaxCall: function (url, data, requestMethod) {

        if (typeof requestMethod === "undefined" || $.isEmptyObject(requestMethod)) requestMethod = "POST";

        return jQuery.ajax({
            type: requestMethod,
            url: url,
            data: data,
            dataType: "json"
        });
    },
    ajaxCallContent: function (url, data) {
        return $.get(url, data || {});
    },
    previewImage: function (file, imageElements) {
        try {
            for (var i = 0; i < imageElements.length; i++) {
                imageElements[i].attr("src", URL.createObjectURL(file));
            }
        } catch (e) {
            var reader = new FileReader();
            reader.onload = function (e) {
                for (var i = 0; i < imageElements.length; i++) {
                    imageElements[i].attr('src', e.target.result);
                }
            };
            reader.readAsDataURL(file);
        }
    },
    handleCKEditor: function (l, a) {

        if (typeof (CKEDITOR) === "undefined") return null;

        if ($.isEmptyObject(a)) return _getInstance(l);
        else if (a === 'get') return _getData(l);

        function _getInstance(l) {
            if (CKEDITOR.instances[l]) return CKEDITOR.instances[l];
            else return CKEDITOR.replace(l);
        }

        function _getData(l) {
            var editor = CKEDITOR.instances[l];

            if ($.isEmptyObject(editor)) return null;

            return editor.getData();
        }
    },
    printPage: function (url) {

        if (typeof (url) === "undefined" || $.isEmptyObject(url)) return;

        var w = window.open(url, '', 'width=250,height=350');
        w.focus();
        w.print();
        setTimeout(function () { w.close(); }, 3000);
    },
    isMobile: {
        Android: function () {
            return navigator.userAgent.toLowerCase().indexOf("android") > -1;
        },
        /*BlackBerry: function() {
         return navigator.userAgent.toLowerCase().indexOf("blackberry");
         },*/
        iOSAll: function () {
            return (
                (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
                (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
                (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
            );
        },
        iOSPhone: function () {
            return (
                (navigator.userAgent.toLowerCase().indexOf("ipad") > -1)
            );
        },
        /*Opera: function() {
         return navigator.userAgent.match(/Opera Mini/i);
         },
         Windows: function() {
         return navigator.userAgent.match(/IEMobile/i);
         },*/
        phone: function () {
            return (ESAWUtilitiesJS.isMobile.Android() || ESAWUtilitiesJS.isMobile.iOSPhone());
        },
        all: function () {
            return (ESAWUtilitiesJS.isMobile.Android() || ESAWUtilitiesJS.isMobile.iOSAll());
        }
    }
};

var ESAWPlugins = {
    fileUpload: function (sel, messenger) {

        var that = this;

        this.sel = sel;

        this.$ = function () { return $((sel || 'data[fn=fileupload]')); }();
        
        this.init = function () {
            this.$.fileupload({
                add: function (e, data) {

                    data.context = new _(that.$/*$ele, $($ele.data('context'))*/, messenger);

                    data.context.onAdd(e, data);

                    data.context.$fileuploadUpload.off('click').on('click', function () {
                        data.context.onUpload(e, data);
                    });

                    data.context.$fileuploadCancel.off('click').on('click', function () {
                        data.context.onAbort(e, data);
                    });
                },
                submit: function (e, data) { },
                progress: function (e, data) {
                    data.context.onProgress(e, data);
                },
                fail: function (e, data) {
                    data.context.onFail(e, data);
                },
                done: function (e, data) {
                    try {
                        data.context.onDone(e, data);
                    } catch (e) { data.context.onFail(e, data); }
                }
            });
        };

        //var $ele = $((sel || 'data[fn=fileupload]'));

        function _($ele, messenger) {

            var that = this;

            this.$ele = $ele;
            this.$context = $($ele.data('context'));//$context;
            this.messenger = messenger;

            this.$fileuploadThumbnail = $("#fileupload-image-thumbnail", this.$context);
            this.$fileuploadPreview = $('#fileupload-image-preview', this.$context);
            this.$fileuploadUpload = $('#fileupload-upload', this.$context);
            this.$fileuploadCancel = $('#fileupload-cancel', this.$context);
            this.$fileuploadPogressBarWrapper = $("#fileupload-progress-wrapper", this.$context);
            this.$fileuploadPogressBar = $('#fileupload-progress-wrapper > .progress > .progress-bar', this.$context);
            this.$fileuploadImage = $('#fileupload-image', this.context);

            this.$fileuploadImageDependent = $(this.$ele.data('fileupload-dependent-selector'));

            this.onAdd = function (e, data) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    that.$fileuploadPreview.html('<img src="' + e.target.result + '" ' + (that.$fileuploadPreview.css('max-height') != 'none' ? 'style="max-height: ' + that.$fileuploadPreview.css('max-height') + ';"' : '') + ' />');
                    that.$fileuploadThumbnail.hide();
                    that.$fileuploadPreview.show();
                    that.$context.addClass('fileupload-exists').removeClass('fileupload-new');
                }

                reader.readAsDataURL(data.files[0]);
            };

            this.onUpload = function (e, data) {
                this.$fileuploadPogressBarWrapper.show(); data.submit();
            };

            this.onDone = function (e, data) {
                var obj = data.result;

                this.messenger.showMessage(obj.Messages[0]);

                if (obj.Result != 'Ok') return;

                var time = (new Date()).getTime();

                var imageUrl = (obj.File.ThumbnailUrl + "?t=" + time);
                this.$fileuploadImage.attr('src', imageUrl);

                if (this.$fileuploadImageDependent.length > 0)
                    this.$fileuploadImageDependent.attr('src', imageUrl);

                this.$fileuploadPogressBar.removeAttr('style');
                this.$fileuploadPogressBarWrapper.hide();
                this.$fileuploadPreview.hide();
                this.$fileuploadThumbnail.show();
                this.$context.addClass('fileupload-new').removeClass('fileupload-exists');
            };

            this.onAbort = function (e, data) {
                data.abort();
                this.$fileuploadThumbnail.show();
                this.$fileuploadPreview.hide();
                this.$fileuploadPreview.empty();
                this.$context.addClass('fileupload-new').removeClass('fileupload-exists');
            };

            this.onProgress = function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                this.$fileuploadPogressBar.css('width', (progress + '%')).attr('aria-valuenow', progress);  
            };

            this.onFail = function (e, data) {
                this.messenger.showMessage({ MessageType: 'Warning', Message: 'Error uploading file' });

                this.$fileuploadPogressBar.removeAttr('style');
                this.$fileuploadPogressBarWrapper.hide();
                this.$fileuploadThumbnail.show();
                this.$fileuploadPreview.hide();
                this.$context.addClass('fileupload-new').removeClass('fileupload-exists');
            };
        }
    },
    select2: function (sel, options) {

        this.sel = sel;

        this.$ = function () { return $(sel); }();

        this.init = function () {
            $(sel).select2(_(options));
        }

        function _(options) {
            var defaults = {};
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        }
    },
    select2Ajax: function (sel, options, selectedHandler, selectedArguments, removedHandler, removedArguments) {

        this.sel = sel;

        this.$ = function () { return $(sel); }();

        this.init = function () {
            $(sel).select2(_(options))
                .on('selected', function (e) {
                    if (selectedHandler) {
                        var _arguments = $.extend([], selectedArguments);
                        _arguments.push(e.val);
                        selectedHandler.apply(this, _arguments);
                    }
                })
                .on("removed", function (e) {
                    if (removedHandler) {
                        var _arguments = $.extend([], removedArguments);
                        _arguments.push(e.val);
                        removedHandler.apply(this, _arguments);
                    }
                });
        }
        
        function _(options) {
            var defaults = {};
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        }
    },
    colorbox: function (sel, options) {

        this.sel = sel;

        this.$ = function () { return $(sel); }();

        this.options = _(options);

        this.init = function () {

            /*$('body').on('click', '.group1', function () {
                $.colorbox({
                    href: $(this).attr('href'), open: true, rel: 'group1',
                    transition: "none",
                    width: "100%",
                    height: "100%",
                    retinaImage: true });
                return false;
            });*/


            $(sel).colorbox(this.options/*{
                rel: 'group1',
                transition: "none",
                width: "100%",
                height: "100%",
                retinaImage: true
            }*/);
        };

        this.destroy = function() {
            $.colorbox.remove()
        };

        function _(options) {
            var defaults = {
                //rel: 'group1',
                transition: "none",
                width: "100%",
                height: "100%",
                retinaImage: true
            };
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        }
    },
    dataTable: function (sel, options) {
        var that = this;

        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.$body = function () { return $(sel + " tbody"); }();

        this.options = _(options);
        this.api = undefined;

        this.init = function () {
            this.$.DataTable(this.options);
            this.api = this.$.dataTable().api();
            this.loadComplement();
        };

        this.loadComplement = function () {
            $(this.sel + '_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");
            // modify table search input
            $(this.sel + '_wrapper .dataTables_length select').addClass("m-wrap small");
            // modify table per page dropdown
            $(this.sel + '_wrapper .dataTables_length select').select2();
            // initialzie select2 dropdown
            $(this.sel + '_wrapper input[type="checkbox"]').change(function () {
                /* Get the DataTables object again - this is not a recreation, just a get of the object */
                var iCol = parseInt($(this).attr("data-column"));
                var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
                oTable.fnSetColumnVis(iCol, (bVis ? false : true));
            });
        };

        this.destroy = function () {
            if (!this.isDataTable()) return;
            this.api.destroy();
        };

        this.isDataTable = function () {
            return $.fn.DataTable.isDataTable(this.$.selector);
        };

        this.clearBody = function () {
            this.$body.empty();
        };

        function _(options) {
            var defaults = {
                "aoColumnDefs": [{
                    "aTargets": [0]
                }],
                "oLanguage": {
                    "sLengthMenu": "Show _MENU_ Rows",
                    "sSearch": "",
                    "oPaginate": {
                        "sPrevious": "",
                        "sNext": ""
                    }
                },
                "aaSorting": [
                    [1, 'asc']
                ],
                "aLengthMenu": [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
            };
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        }

        (function () {
            $.extend(true, $.fn.dataTable.defaults, {
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap"
            });

            /* Default class modification */
            $.extend($.fn.dataTableExt.oStdClasses, {
                "sWrapper": "dataTables_wrapper form-inline"
            });

            /* API method to get paging information */
            $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": oSettings._iDisplayLength === -1 ?
                        0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": oSettings._iDisplayLength === -1 ?
                        0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            /* Bootstrap style pagination control */
            $.extend($.fn.dataTableExt.oPagination, {
                "bootstrap": {
                    "fnInit": function (oSettings, nPaging, fnDraw) {
                        var oLang = oSettings.oLanguage.oPaginate;
                        var fnClickHandler = function (e) {
                            e.preventDefault();
                            if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                                fnDraw(oSettings);
                            }
                        };

                        $(nPaging).append(
                            '<ul class="pagination">' +
                            '<li class="prev disabled"><a href="#"><i class="icon-double-angle-left"></i> ' + oLang.sPrevious + '</a></li>' +
                            '<li class="next disabled"><a href="#">' + oLang.sNext + ' <i class="icon-double-angle-right"></i></a></li>' +
                            '</ul>'
                        );
                        var els = $('a', nPaging);
                        $(els[0]).bind('click.DT', { action: "previous" }, fnClickHandler);
                        $(els[1]).bind('click.DT', { action: "next" }, fnClickHandler);
                    },

                    "fnUpdate": function (oSettings, fnDraw) {
                        var iListLength = 5;
                        var oPaging = oSettings.oInstance.fnPagingInfo();
                        var an = oSettings.aanFeatures.p;
                        var i, ien, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

                        if (oPaging.iTotalPages < iListLength) {
                            iStart = 1;
                            iEnd = oPaging.iTotalPages;
                        }
                        else if (oPaging.iPage <= iHalf) {
                            iStart = 1;
                            iEnd = iListLength;
                        } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                            iStart = oPaging.iTotalPages - iListLength + 1;
                            iEnd = oPaging.iTotalPages;
                        } else {
                            iStart = oPaging.iPage - iHalf + 1;
                            iEnd = iStart + iListLength - 1;
                        }

                        for (i = 0, ien = an.length; i < ien; i++) {
                            // Remove the middle elements
                            $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                            // Add the new list items and their event handlers
                            for (j = iStart; j <= iEnd; j++) {
                                sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                                $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                                    .insertBefore($('li:last', an[i])[0])
                                    .bind('click', function (e) {
                                        e.preventDefault();
                                        oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                                        fnDraw(oSettings);
                                    });
                            }

                            // Add / remove disabled classes from the static elements
                            if (oPaging.iPage === 0) {
                                $('li:first', an[i]).addClass('disabled');
                            } else {
                                $('li:first', an[i]).removeClass('disabled');
                            }

                            if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                                $('li:last', an[i]).addClass('disabled');
                            } else {
                                $('li:last', an[i]).removeClass('disabled');
                            }
                        }
                    }
                }
            });

            /*
             * TableTools Bootstrap compatibility
             * Required TableTools 2.1+
             */
            if ($.fn.DataTable.TableTools) {
                // Set the classes that TableTools uses to something suitable for Bootstrap
                $.extend(true, $.fn.DataTable.TableTools.classes, {
                    "container": "DTTT btn-group",
                    "buttons": {
                        "normal": "btn",
                        "disabled": "disabled"
                    },
                    "collection": {
                        "container": "DTTT_dropdown dropdown-menu",
                        "buttons": {
                            "normal": "",
                            "disabled": "disabled"
                        }
                    },
                    "print": {
                        "info": "DTTT_print_info modal"
                    },
                    "select": {
                        "row": "active"
                    }
                });

                // Have the collection use a bootstrap compatible dropdown
                $.extend(true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
                    "collection": {
                        "container": "ul",
                        "button": "li",
                        "liner": "a"
                    }
                });
            }
        })();
    },
    datePicker: function (sel, options) {

        this.sel = sel;

        this.$ = function () { return $(sel); }();

        this.init = function () {
            $(sel).datepicker(_(options));
        };

        function _(options) {
            var defaults = { autoclose: true };
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        }
    },

    dropZone: function (sel, options) {
        var that = this;
        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.instance = undefined;
        this.init = function () {
            this.instance = new Dropzone(this.sel, _(options));
        };

        function _(options) {
            var defaults = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10.0, // MB
                addRemoveLinks: true,
            };
            return typeof options === "undefined" ? defaults : $.extend({}, defaults, options);
        };
        //_ESAW.roomMediaDropzone.instance.
        this.uploadAllFiles = function() {
            this.instance.processQueue();
        };

        this.uploadSingleFile = function (index) {
            this.instance.processFile(this.instance.getQueuedFiles()[index])
        };
    }
};

var ESAWConsts = {
    ajaxErrorCode: 555,
    ajaxSessionTimeoutCode: 556
}

var ESAWForms = {
    basic: function (sel, actionUrl, messenger, ajaxLoader, callback) {
        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.action = actionUrl;
        this.sr = function () { return $(sel).serialize(); };

        this.messenger = messenger;
        this.ajaxLoader = ajaxLoader;
        this.callback = callback;
    },
    single: function (sel, actionUrl, messenger, ajaxLoader, callback) {
        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.action = actionUrl;

        this.messenger = messenger;
        this.ajaxLoader = ajaxLoader;
        this.callback = callback;
    },
    delete: function (actionUrl, messenger, ajaxLoader, callback) {
        this.action = actionUrl;
        this.messenger = messenger;
        this.ajaxLoader = ajaxLoader;
        this.callback = callback;
    },
    load: function (sel, actionUrl, messenger, ajaxLoader, callback) {
        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.action = actionUrl;

        this.messenger = messenger;
        this.ajaxLoader = ajaxLoader;
        this.callback = callback;
    },
    loadForm: function (sel, actionUrl, messenger, ajaxLoader, extra) {

        this.form = [];

        this.sel = sel;
        this.$ = function () { return $(sel); }();
        this.action = actionUrl;

        this.addFormItem = function (fieldSelector, fieldType, propertyNameForValue) {
            this.form.push({ fieldSelector: fieldSelector, fieldType: fieldType, propertyNameForValue: propertyNameForValue });
        };

        this.bindModel = function (model) {
            for (var i = 0; i < this.form.length; i++) {
                var field = this.form[i];

                if ("label" === field.fieldType) {
                    $(field.fieldSelector).text(model[field.propertyNameForValue]);
                }
                else if ("text" === field.fieldType || "hidden" === field.fieldType) {
                    $(field.fieldSelector).val(model[field.propertyNameForValue]);
                }
            }
        };

        this.clearForm = function () {
            for (var i = 0; i < this.form.length; i++) {
                var field = this.form[i];

                if ("label" === field.fieldType) {
                    $(field.fieldSelector).text('');
                }
                else if ("text" === field.fieldType || "hidden" === field.fieldType) {
                    $(field.fieldSelector).val('');
                }
            }
        };

        this.messenger = messenger;
        this.ajaxLoader = ajaxLoader;

        this.extra = extra;
    }
}

var ESAWMessengers = {
    basic: function (wrapperSel, timeout) {
        this.showMessage = function (dataMessage) {

            var messageType = dataMessage.MessageType;
            var message = dataMessage.Message;

            var html = _();
            var identifier = (new Date()).getTime();

            switch (messageType) {
                case "Success":
                    html = String.format(html, identifier, 'alert-success', 'fa fa-check-circle', 'Well done!', message);
                    break;
                case "Error":
                    html = String.format(html, identifier, 'alert-danger', 'fa fa-times-circle', 'Oh snap!', message);
                    break;
                case "Warning":
                    html = String.format(html, identifier, 'alert-warning', 'fa fa-exclamation-triangle', 'Warning!', message);
                    break;
                case "Info":
                default:
                    html = String.format(html, identifier, 'alert-info', 'fa fa-info-circle', 'Heads up!', message);
                    break;
            };

            $(wrapperSel).append(html).show();

            setTimeout(function () {
                $(wrapperSel).find("div#" + identifier).remove();
            }, (timeout == undefined ? 5000 : timeout));

            function _() {
                return  '<div id="{0}" class="alert {1} alert-dismissible">' +
                            //'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                            '<i class="fa {2}"></i> ' +
                            '<strong>{3}</strong> {4}' +
                        '</div>';
            }
        }
    },
    alert: function () {
        this.showMessage = function (message) {
            alert(message);
        }
    },
    debug: function () {
        this.showMessage = function(message) {
            console.log(message);
        }
    }
};

var ESAWAjaxLoader = {
    basic: function (wrapperSel, className) {

        this.wrapperSel = wrapperSel;
        this.className = className;

        this.show = function() {
            $(this.wrapperSel).append("<div class='" + this.className + "'></div>");
        };
        this.hide = function() {
            $(this.wrapperSel).find("." + this.className).remove();
        };
    },
    withMessage: function (wrapperSel, className, message) {

        this.wrapperSel = wrapperSel;
        this.className = className;
        this.message = message;

        this.show = function () {
            $(this.wrapperSel).append("<div class='" + this.className + "'><div>" + this.message + "</div></div>");
        };
        this.hide = function () {
            $(this.wrapperSel).find("." + this.className).remove();
        };
    }
};

var ESAWAjaxAware = (function () {

    var subjects = {};

    function subscribe(subject, loader) {

        if (subjects[subject]) throw "An ajax call is currently active"

        if (loader) loader.show();

        subjects[subject] = { subject: subject, loader: loader };
    }

    function unsubscribe(subject) {
        if (!subject || !subjects[subject]) return;


        if (subjects[subject].loader) subjects[subject].loader.hide();

        delete subjects[subject];
    }

    return {
        subscribe: subscribe,
        unsubscribe: unsubscribe
    };

})();

var ESAWModals = {
    
    confirm: function (sel) {

        var that = this;

        this.sel = sel;

        this.$ = function () {
            return $(sel);
        }();

        this.show = function () {
            this.$.modal("show");
        };

        this.hide = function () {
            this.$.modal("hide");
        };

        this.setHandlers = function (okHandlerString, cancelHandlerString) {
            if (!okHandlerString) okHandlerString = "";
            if (!cancelHandlerString) cancelHandlerString = "";

            if (okHandlerString) this.$.find("#btnYes").attr("onclick", okHandlerString);
            if (!cancelHandlerString) this.$.find("#btnNo").attr("onclick", cancelHandlerString);

            this.$.off('hidden.bs.modal').on('hidden.bs.modal', function () {
                that.$.find("#btnYes").removeAttr("onclick");
                that.$.find("#btnNo").removeAttr("onclick");
            });
        }
    },
    confirmWithDynamicContent: function (sel, contentWrapper) {

        var that = this;

        this.sel = sel;

        this.$ = function () {
            return $(sel);
        }();

        this.$contentWrapper = function () {
            return $(contentWrapper);
        }();

        this.show = function () {
            this.$.modal("show");
        };

        this.hide = function () {
            this.$.modal("hide");
        };

        this.setHandlers = function (okHandlerString, cancelHandlerString) {
            if (!okHandlerString) okHandlerString = "";
            if (!cancelHandlerString) cancelHandlerString = "";

            if (okHandlerString) this.$.find("#btnYes").attr("onclick", okHandlerString);
            if (!cancelHandlerString) this.$.find("#btnNo").attr("onclick", cancelHandlerString);

            this.$.off('hidden.bs.modal').on('hidden.bs.modal', function () {
                that.$.find("#btnYes").removeAttr("onclick");
                that.$.find("#btnNo").removeAttr("onclick");
                that.clearContent();
            });
        };

        this.clearContent = function () {
            this.$contentWrapper.empty();
        };

        
        (function () {
            $(sel).on('hidden.bs.modal', function () {
                that.clearContent();
            });
        })();
    }
};

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

window["isId"] = function (value) { return !isNaN(value) && value > 0; }