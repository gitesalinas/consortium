﻿; (function ($, window, document, undefined, util, ajaxAware) {
    var params = {};

    var validations = {
        login: {
            rules: {
                "Username": { required: true },
                "Password": { required: true }
            },
            messages: {
                "Username": { required: "Username is required" },
                "Password": { required: "Password is required" }
            }
        },
        register: {
            rules: {
                "Firstname": { required: true },
                "Lastname": { required: true },
                "Email": { required: true },
                "DocumentNumber": { required: true },
                "Username": { required: true },
                "Password": { required: true },
                "ConfirmPassword": { equalTo: "#Register_Password" }
            },
            messages: {
                "Firstname": { required: "Firstname is required" },
                "Lastname": { required: "Lastname is required" },
                "Email": { required: "Email is required" },
                "DocumentNumber": { required: "Document Number is required" },
                "Username": { required: "Username is required" },
                "Password": { required: "Password is required" },
                "ConfirmPassword": { equalTo: "Please enter the same value of Password Field" }
            }
        }
    };

    var functions = {
        _init: function () {
            this._bind();
            this._plugin();
        },
        _bind: function () {

        },
        _plugin: function () {
            params.loginForm.$.validate(validations.login);
            params.registerForm.$.validate(validations.register);
        },
        _login: function ($ele) {

            ajaxAware.subscribe($ele, params.loginForm.ajaxLoader);

            var ajax = util.ajaxCall(params.loginForm.action, params.loginForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                params.loginForm.messenger.showMessage(data.Messages[0]);

                setTimeout(function () { location.href = data.RedirectTo; }, 800);

                params.loginForm.$[0].reset();

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.loginForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele); });

            return false;
        },
        _register: function ($ele) {

            ajaxAware.subscribe($ele, params.registerForm.ajaxLoader);

            var ajax = util.ajaxCall(params.registerForm.action, params.registerForm.sr());

            ajax.done(function (data, textStatus, jqXHR) {

                params.registerForm.messenger.showMessage(data.Messages[0]);

                setTimeout(function () { location.href = data.RedirectTo; }, 800);

                params.registerForm.$[0].reset();

            }).fail(function (jqXHR, textStatus, errorThrown) {
                util.handleAjaxResponseError(jqXHR, textStatus, errorThrown, params.registerForm.messenger);
            }).always(function (data, textStatus, jqXHR) { ajaxAware.unsubscribe($ele); });
        }
    };

    var methods = {
        init: function (parameter) {

            params = $.extend(true, params, parameter);

            functions._init();
        },
        login: function (ele) {

            if (!params.loginForm.$.valid()) {
                return;
            }

            return functions._login($(ele));
        },
        register: function () {

            if (!params.registerForm.$.valid()) {
                return;
            }

            functions._register($(ele));
        }
    };

    $.fn.pageJS = function (parameter) {

        if (methods[parameter]) {
            return methods[parameter].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof parameter === 'object' || !parameter) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };

    $(function () { $.fn.pageJS(_ESAW); });

})(jQuery, window, document, undefined, ESAWUtilitiesJS, ESAWAjaxAware);